let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css')

// Browser Sync

// .browserSync({
//     open: 'external',
//     host: 'example.com',
//     proxy: 'example.com',
//     files: [
//         'resources/assets/sass/*.scss',
//     ]
// });
  //  .copy('node_modules/semantic-ui-css/semantic.min.css','public/vendor/semantic-ui/semantic.min.css')
  //  .copy('node_modules/semantic-ui-css/semantic.min.js','public/vendor/semantic-ui/semantic.min.js');

mix.scripts([
    'resources/assets/js/pages/sidebars/init-leftmenu-tooltip.js',
    'resources/assets/js/pages/sidebars/toggle-rightbar.js',
    'resources/assets/js/pages/page-loader.js',
    'resources/assets/js/pages/form-loader.js',
    'resources/assets/js/pages/init-semantic-widgets.js',
    // 'resources/assets/js/core/Errors.js',
    ], 'public/js/layouts/admin-dashboard.js');

mix.scripts([
    'resources/assets/js/pages/sidebars/init-leftmenu-tooltip.js',
    'resources/assets/js/pages/sidebars/toggle-rightbar.js',
    'resources/assets/js/pages/page-loader.js',
    'resources/assets/js/pages/form-loader.js',
    'resources/assets/js/pages/init-semantic-widgets.js',
    // 'resources/assets/js/core/Errors.js',
    ], 'public/js/layouts/buyer-dashboard.js');

mix.scripts([
  'resources/assets/js/pages/sidebars/init-leftmenu-tooltip.js',
  'resources/assets/js/pages/sidebars/toggle-rightbar.js',
  'resources/assets/js/pages/page-loader.js',
  'resources/assets/js/pages/form-loader.js',
  'resources/assets/js/pages/init-semantic-widgets.js',
  // 'resources/assets/js/core/Errors.js',
  ], 'public/js/layouts/agency-dashboard.js');

mix.scripts([
  'resources/assets/js/pages/page-loader.js',
  'resources/assets/js/pages/form-loader.js',
  'resources/assets/js/pages/init-semantic-widgets.js',
  // 'resources/assets/js/core/Errors.js',
  ], 'public/js/layouts/agency-home.js');

mix.scripts([
  'resources/assets/js/pages/page-loader.js',
  'resources/assets/js/pages/form-loader.js',
  'resources/assets/js/pages/init-semantic-widgets.js',
  // 'resources/assets/js/core/Errors.js',
  ], 'public/js/layouts/access.js');

mix.scripts([
  'resources/assets/js/pages/sidebars/init-leftmenu-tooltip.js',
  'resources/assets/js/pages/sidebars/toggle-rightbar.js',
  'resources/assets/js/pages/page-loader.js',
  'resources/assets/js/pages/form-loader.js',
  'resources/assets/js/pages/init-semantic-widgets.js',
  // 'resources/assets/js/core/Errors.js',
  ], 'public/js/layouts/client-dashboard.js');

mix.scripts([
  'resources/assets/js/pages/page-loader.js',
  'resources/assets/js/pages/form-loader.js',
  'resources/assets/js/pages/init-semantic-widgets.js',
  // 'resources/assets/js/core/Errors.js',
  ], 'public/js/layouts/client-home.js');

mix.scripts([
  'resources/assets/js/pages/page-loader.js',
  // 'resources/assets/js/pages/form-loader.js',
  'resources/assets/js/pages/init-semantic-widgets.js',
  // 'resources/assets/js/core/Errors.js',
  ], 'public/js/layouts/public.js');

