<?php

namespace Tests\Feature;

use Tests\TestCase;
// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class GuestTest extends TestCase
{
    public function test_redirect_to_guest_page_if_not_loggedin()
    {
        $response = $this->get('/');

        $response->assertStatus(302);
    }

    public function test_guest_page_content()
    {
        $response = $this->get('/guest');

        $response->assertStatus(200);

        $response->assertSee('Create your own platform to sell your cleints services and subscriptions');

        $response->assertSee("Products");

        $response->assertSee("Login");

        $response->assertSee("Register");
    }

    public function test_no_user_is_auth()
    {
        $this->assertGuest();
    }
}
