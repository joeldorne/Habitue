<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
// use Illuminate\Foundation\Testing\DatabaseTransactions;

class LoginPageTest extends TestCase
{
    // use DatabaseTransactions, DatabaseMigrations;

    protected $test_agency_user;
    protected $test_agency_user_data = [
        'first_name' => 'test_agency_user_first_name',
        'last_name' => 'test_agency_user_last_name',
        'email' => 'test_agency_user@email.com',
        'password' => 'qwqwqw',
        'role_id' => 2, 
    ];

    protected $test_client_user;
    protected $test_client_user_data = [
        'first_name' => 'test_client_user_first_name',
        'last_name' => 'test_client_user_last_name',
        'email' => 'test_client_user@email.com',
        'password' => 'qwqwqw',
        'role_id' => 3, 
    ];

    
    public function generate_and_get_new_agency_user()
    {
        if ($this->test_agency_user)
            return $this->test_agency_user;

        $this->test_agency_user = factory()->create([$this->test_agency_user_data]);

        return $this->test_agency_user;
    }

    
    public function generate_and_get_new_client_user()
    {
        if ($this->test_client_user)
            return $this->test_client_user;

        $this->test_client_user = factory()->create([$this->test_client_user_data]);

        return $this->test_client_user;
    }


    public function test_login_page_content()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);

        $response->assertSee('Signin to Habitue');
    }


    public function xtestPerformLogin()
    {
        $response = $this->actingAs($this->generateAndGetNewClientUser())
                         ->withSession(['foo' => 'bar'])
                         ->get('/login');

        $this->assertAuthenticated();
    }
}
