<?php

namespace Tests\Feature;

use Tests\TestCase;
// use Illuminate\Foundation\Testing\WithFaker;
// use Illuminate\Foundation\Testing\RefreshDatabase;

class RouteTest extends TestCase
{
    /** @test */
    public function guestRoutes()
    {
        $appURL = env('APP_URL');

        $urls = [
            '/',
            '/guest',
            '/login',
            '/password/reset',
            '/register?role=agency',
            '/register?role=client'
        ];

        echo PHP_EOL;

        foreach ($urls as $url)
        {
            $response = $this->get($url);

            if ( (int)$response->status() !== 200 )
            {
                echo $appURL . $url . ' (FAILED) did not return a 200';
                $this->assertTrue(false);
            }
            else 
            {
                echo $appURL . $url . ' (success ?)';
                $this->assertTrue(true);
            }
            echo PHP_EOL;
        }

        $this->assertTrue(true);
    }
}
