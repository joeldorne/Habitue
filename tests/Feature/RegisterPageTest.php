<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class RegisterPageTest extends TestCase
{
    public function testRegisterPageContentAsAgency()
    {
        $response = $this->get('/register?role=agency');

        $response->assertStatus(200);

        $response->assertSee('Signup to Habitue as Agency');
    }

    public function testRegisterPageContentAsClient()
    {
        $response = $this->get('/register?role=client');

        $response->assertStatus(200);

        $response->assertSee('Signup to Habitue as Client');
    }
}
