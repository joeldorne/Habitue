<?php

/*
|--------------------------------------------------------------------------
| Agency Routes
|--------------------------------------------------------------------------
|
|
*/



Route::get('/create', 'ClientController@create')->name('client.start');
Route::post('/create', 'ClientController@store')->name('client.store');

Route::get('/', 'ClientController@dashboard')->name('client');


/*
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/


Route::group([
  // 'domain' => config('session.domain_url'),
  // 'middleware' => ['clientsubdomain']
], function () { 


  Route::get('/dashboard', 'ClientController@dashboard')->name('client.dashboard');



  Route::get('/profile', 'ClientController@profile')->name('client.profile');
  Route::get('/profile/change', 'ClientController@editprofile')->name('client.profile.edit');



  Route::get('/team', 'ClientTeamController@team')->name('client.team');



  Route::get('/purchases', 'PurchasesController@purchases')->name('client.purchases');



  Route::get('/billings/pending', 'BillingController@pending')->name('client.billings.pending');

  Route::get('/billings/cards', 'BillingCardController@cards')->name('client.billings.cards');
  Route::get('/billings/card/add', 'BillingCardController@addCard')->name('client.billings.cards.add');
  Route::get('/billings/card/{card}/makedefault', 'BillingCardController@makeDefault')->name('client.billings.cards.makedefault');
  Route::get('/billings/card/{card}/remove', 'BillingCardController@remove')->name('client.billings.cards.remove');


});


/*
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/



Route::post('/team/invite', 'ClientTeamController@teamInviteStore')->name('client.team.invite.store');



Route::post('/profile/change', 'ClientController@updateprofile')->name('client.profile.update');



Route::post('/billings/cards/add', 'BillingCardController@storeCard')->name('client.billings.cards.store');