<?php

/*
|--------------------------------------------------------------------------
| Agency Routes
|--------------------------------------------------------------------------
|
|
*/


// testing route
Route::get('/product/list', 'ProductController@index')->name('products.list');

Route::get('/all', 'ProductController@listOfAgentsPublicLink')->name('products.all');;

Route::get('/items/{subdomain}', 'ProductController@products')->name('products');

Route::get('/item/{agency_product}', 'ProductController@product')->name('products.product');

Route::get('/purchasing/{agency_product}', 'ProductController@purchasing')->name('products.purchasing');

Route::post('/purchased', 'ProductController@purchased')->name('products.purchased');








/*
Route::group([
  'domain' => config('session.domain_url'),
], function () {


  Route::get('/product/list', 'ProductController@index')->name('products.list');

  Route::get('/products', 'ProductController@products')->name('products');

  Route::get('/product/{agency_product}', 'ProductController@product')->name('products.product');

  Route::get('/purchasing/{agency_product}', 'ProductController@purchasing')->name('products.purchasing');

  Route::post('/purchased', 'ProductController@purchased')->name('products.purchased');

});
*/


// Route::get('/product/list', 'ProductControllers@index')->name('products.list');