<?php

/*
|--------------------------------------------------------------------------
| Agency Routes
|--------------------------------------------------------------------------
|
|
*/



Route::get('/', 'AdminController@index')->name('admin');
Voyager::routes();



Route::get('/products', 'ProductController@list')->name('admin.products');
Route::get('/product/create', 'ProductController@create')->name('admin.product.create');
Route::get('/product/view/{product}', 'ProductController@view')->name('admin.product.view');
Route::get('/product/edit/{product}', 'ProductController@edit')->name('admin.product.edit');

Route::post('/product/store/basic', 'ProductController@storeBasic')->name('admin.product.store.basic');
Route::post('/product/update/basic', 'ProductController@updateBasic')->name('admin.product.update.basic');
Route::post('/product/update/image', 'ProductController@updateImage')->name('admin.product.update.image');
Route::post('/product/update/paymentmethod', 'ProductController@updatePaymentMethod')->name('admin.product.update.paymentmethod');
Route::post('/product/update/pricing/singular', 'ProductController@updatePricingSingular')->name('admin.product.update.pricing.singular');
Route::post('/product/update/pricing/variation', 'ProductController@updatePricingVariation')->name('admin.product.update.pricing.variation');
Route::post('/product/update/activationformurl', 'ProductController@updateActivationFormUrl')->name('admin.product.update.activationformurl');
Route::post('/product/update/visibility', 'ProductController@updateVisibility')->name('admin.product.update.visibility');

// Route::post('/product/store', 'ProductController@store')->name('admin.product.store');
// Route::post('/product/update', 'ProductController@update')->name('admin.product.update');
// Route::post('/product/store/image', 'ProductController@storeImage')->name('admin.product.store.image');

Route::get('/product/settoactive/{product}', 'ProductController@setToActive')->name('admin.product.settoactive');
Route::get('/product/settoinactive/{product}', 'ProductController@setToInactive')->name('admin.product.settoinactive');
  


Route::get('/payment-reports', 'PaymentReportController@sales')->name('voyager.payment-reports.sales');
Route::post('/payment-reports/purchase/updatecompletion', 'PaymentReportController@updateCompletion')->name('admin.payment-report.purchase.updatecompletion');
Route::post('/payment-reports/purchase/updatestatus', 'PaymentReportController@updateStatus')->name('admin.payment-report.purchase.updatestatus');



Route::get('/subscriptions', 'SubscriptionController@list')->name('admin.subscriptions');
Route::get('/subscriptions/create', 'SubscriptionController@create')->name('admin.subscription.create');
Route::get('/subscription/view/{product_service}', 'SubscriptionController@view')->name('admin.subscription.view');
Route::get('/subscription/edit/{product_service}', 'SubscriptionController@edit')->name('admin.subscription.edit');

Route::post('/subscription/store/basic', 'SubscriptionController@storeBasic')->name('admin.subscription.store.basic');
Route::post('/subscription/update/basic', 'SubscriptionController@updateBasic')->name('admin.subscription.update.basic');
Route::post('/subscription/update/image', 'SubscriptionController@updateImage')->name('admin.subscription.update.image');
Route::post('/subscription/fetch/plans', 'SubscriptionController@fetchPlans')->name('admin.subscription.fetch.plans');
Route::post('/subscription/store/plan', 'SubscriptionController@storePlan')->name('admin.subscription.store.plan');
Route::post('/subscription/update/plan', 'SubscriptionController@updatePlan')->name('admin.subscription.update.plan');
Route::post('/subscription/remove/plan', 'SubscriptionController@removePlan')->name('admin.subscription.remove.plan');
Route::post('/subscription/update/visibility', 'SubscriptionController@updateVisibility')->name('admin.subscription.update.visibility');



Route::get('/settings/stripe', 'StripeManageController@migration')->name('admin.settings.stripe.migration');
Route::get('/settings/stripe/migrate-product-subscriptions', 'StripeManageController@migrateProductService')->name('admin.settings.stripe.migration.productService');



//
Route::get('/test1', 'PaymentReportController@test1');