<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Auth;

use App\ClientMemberInvite;

class CheckEmailIfAlreadyInvitedOnClientMembers implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (ClientMemberInvite::ForClient(Auth::user()->client[0]->id)->WhereEmail($value)->exists())
        {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute `:input` is already been invited.';
    }
}
