<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use App\User;

class CheckEmailOnClientMembers implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::WhereEmail($value)->first();

        // check if email is already registered
        if (!$user)
            return true;

        // check if user is already joined an client
        if ($user->client->count() < 1)
            return true;

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute `:input` is already a member on '.config('app.name').'.';
    }
}
