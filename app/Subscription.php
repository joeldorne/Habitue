<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Subscription extends Model
{
    protected $table = 'subscriptions';
    protected $dates = [
        'trial_ends_at', 
        'ends_at', 
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $hidden = [
        'user_id', 
        'stripe_id', 
        'stripe_plan', 
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];


    public function subscriber()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


    public function scopeSubscribed($query)
    {
        // return $query->whereNull('ends_at')->orWhere('ends_at', '>', Carbon::now())->orWhereNotNull('trial_ends_at')->where('trial_ends_at', '>', Carbon::today());
        // return $query->where('ends_at', null);
        return $query->whereNull('ends_at');
    }

    public function scopeSubscribedxxx($query)
    {
        return $query->whereHas('user', function ($r) {
            $r->whereHas('subscriptions', function ($s) {
                $s->whereNested(function ($t) {
                    $t->where('name', 'main') // name of subscription
                        ->whereNull('ends_at')
                        ->orWhere('ends_at', '>', Carbon::now())
                        ->orWhereNotNull('trial_ends_at')
                        ->where('trial_ends_at', '>', Carbon::today());
                });
            });
        });
    }
}
