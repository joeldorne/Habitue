<?php

namespace App\Traits;


Trait Purchasable
{
    /**
     * Get all purchases from this model
     */
    public function mypurchases() 
    {
        return $this->morphMany('App\Purchase', 'purchasable');
    }


    // public function purchases()
    // {
    //     return $this->hasMany('App\Purchase');
    // }


    // public function product_purchases()
    // {
    //     return $this->hasMany('App\Purchase')->with('product');
    // }
}
