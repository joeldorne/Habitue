<?php

namespace App\Traits;


Trait AgencyGroup
{
    public function agency() 
    {
        return $this->belongsToMany('App\Agency')->withPivot('position')->withTimestamps();
    }



    public function myAgency()
    {
        return $this->agency[0];
    }

    // public function myGroup()
    // {
    //     return $this->agency[0];
    // }
}
