<?php

namespace App\Traits;

trait ScopesForVisibilityAndEnability
{
    protected $STATUS_ENABLED = false;
    protected $STATUS_DISABLED = true;

    protected $STATUS_VISIBLE = false;
    protected $STATUS_HIDDEN = true;

    public function scopeEnabled($query) 
    {
        return $query->where('disabled', '=', $this->STATUS_ENABLED);
    }

    public function scopeDisabled($query) 
    {
        return $query->where('disabled', '=', $this->STATUS_DISABLED);
    }

    public function scopeVisible($query) 
    {
        return $query->where('hide', '=', $this->STATUS_VISIBLE);
    }

    public function scopeHidden($query) 
    {
        return $query->where('hide', '=', $this->STATUS_HIDDEN);
    }

    public function scopeActive($query)
    {
        return $query->where('disabled', $this->STATUS_ENABLED)->where('hide', $this->STATUS_VISIBLE);
    }

    public function scopeValid($query)
    {
        return $query->where('disabled', $this->STATUS_ENABLED)->where('hide', $this->STATUS_VISIBLE);
    }
}