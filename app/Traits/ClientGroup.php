<?php

namespace App\Traits;


Trait ClientGroup
{
    public function client() 
    {
        return $this->belongsToMany('App\Client')->withPivot('position')->withTimestamps();
    }



    public function myClient()
    {
        return $this->client[0];
    }

    // public function myGroup()
    // {
    //     return $this->agency[0];
    // }
}
