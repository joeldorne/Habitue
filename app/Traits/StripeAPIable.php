<?php

namespace App\Traits;

use Auth;


trait StripeAPIable
{
    /**
     * DB table relationship
     */
    public function stripe() 
    {
        return $this->morphOne('App\Stripe', 'stripecustomerable');
    }

    /**
     * Check if group has an existing stripe API Object
     * 
     * @return bool
     */
    public function checkStripeAPI()
    {
        $stripeCustomer = $this->stripeCustomer;

        if (!$stripeCustomer)
            return false;

        $stripeAPI = $this->getStripeAPICustomer();

        if (!$stripeAPI)
            return false;

        return true;
    }

    /**
     * Get the group's \Stripe\Customer API Object
     * 
     * Create new \Stripe\Customer if not yet exists
     * 
     * @return \Stripe\Customer API Object if exists
     * @return bool [false] if fail
     */
    public function getStripeAPI()
    {
        if (!$this->stripeCustomer)
        {
            return $this->createStripeAPICustomer();
        }

        return $this->getStripeAPICustomer();
    }

    public function addCard($token)
    {
        if (!$this->checkStripeAPI())
            return false;

        return $this->addStripeAPICard($token);
    }

    public function makeDefaultCard($card_id)
    {
        if (!$this->checkStripeAPI())
            return false;

        return $this->changeStripeAPICardDefault($card_id);
    }

    public function removeCard($card_id)
    {
        if (!$this->checkStripeAPI())
            return false;

        return $this->removeStripeAPICard($card_id);
    }

    public function payUsingExistingCard($card_id, \App\Product $product, $price)
    {
        if (!$this->checkStripeAPI())
            return false;

        return $this->payWithStripeAPICard($card_id, $product, $price);
    }

    /**
     * Get the group's \Stripe\Customer API Object
     * 
     * @return \Stripe\Customer API Object if exists
     * @return bool [false] if fail
     */
    protected function getStripeAPICustomer() 
    {
        $stripeCustomer = $this->stripeCustomer;

        if ($stripeCustomer) 
        {
            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

            try {
                return \Stripe\Customer::retrieve($stripeCustomer->stripe_id);
            }
            catch(\Stripe\Error\Card $e) {
                return false;
    
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Card-error-msg-';
            } 
            catch (\Stripe\Error\RateLimit $e) {
                return false;
    
                // Too many requests made to the API too quickly
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Ratelimit-error-msg-';
            } 
            catch (\Stripe\Error\InvalidRequest $e) {
                return false;
    
                // invalid customer id provided
                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-invalidRequest-error-msg-';
            } 
            catch (\Stripe\Error\Authentication $e) {
                return false;
    
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Authentication-error-msg-';
            } 
            catch (\Stripe\Error\ApiConnection $e) {
                return false;
    
                // Network communication with Stripe failed
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-ApiConnection-error-msg-';
            } 
            catch (\Stripe\Error\Base $e) {
                return false;
    
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Base-error-msg-';
            } 
            catch (Exception $e) {
                return false;
    
                // Something else happened, completely unrelated to Stripe
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Exception-error-msg-';
            }
        }
        else 
        {
            return false;
        }
    }

    /**
     * Create group's \Stripe\Customer API Object
     * 
     * @return \Stripe\Customer if exists
     * @return bool [false] if fail
     */
    protected function createStripeAPICustomer()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        try {
            $customer = \Stripe\Customer::create([
                'email' => Auth::user()->email,
                'description' => '',
                'metadata' =>  array(
                    "user_id" => Auth::user()->id,
                    "group_type" => Auth::user()->role->name,
                    "group_id" => $this->id,
                    "group_name" => $this->name,
                ),
            ]);
        }
        catch(\Stripe\Error\Card $e) {
            return false;

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return false;

            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return false;

            // invalid customer id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return false;

            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return false;

            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return false;

            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return false;

            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }

        $this->stripeCustomer()->create([
            'user_id' => Auth::user()->id,
            'stripe_id' => $customer->id,
        ]);

        return $customer;
    }

    protected function addStripeAPICard($token)
    {
        $customer = $this->getStripeAPICustomer();

        if (!$customer)
            return false;

        try {
            $customer->sources->create(array(
                "source" => $token,
            ));
        }
        catch(\Stripe\Error\Card $e) {
            return false;

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return false;

            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return false;

            // invalid card id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return false;

            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return false;

            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return false;

            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return false;

            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }

        return true;
    }

    protected function changeStripeAPICardDefault($card_id)
    {
        $customer = $this->getStripeAPICustomer();

        if (!$customer)
            return false;

        try {
            $customer->default_source = $card_id;
            $customer->save();
        }
        catch(\Stripe\Error\Card $e) {
            return false;

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return false;

            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return false;

            // invalid card id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return false;

            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return false;

            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return false;

            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return false;

            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }

        return true;
    }

    protected function removeStripeAPICard($card_id)
    {
        $customer = $this->getStripeAPICustomer();

        if (!$customer)
            return false;

        if ($customer->default_source == $card_id)
            return false;

        try {
            $customer->sources->retrieve($card_id)->delete();
        }
        catch(\Stripe\Error\Card $e) {
            return false;

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return false;

            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return false;

            // invalid card id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return false;

            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return false;

            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return false;

            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return false;

            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }

        return true;
    }

    protected function payWithStripeAPICard($card_id, \App\Product $product, $price)
    {
        $customer = $this->getStripeAPICustomer();

        if (!$customer)
            return false;

        try {
            // Charge the user's card:
            $charge = \Stripe\Charge::create(array(
                "amount" => ($price * 100),
                "currency" => "usd",
                "description" => $product->description,
                "statement_descriptor" => "Custom descriptor", // Statement descriptors are limited to 22 characters, cannot use the special characters <, >, ', or ", and must not consist solely of numbers.
                "customer" => $customer->id,
                "source" => $card_id,
                // "capture" => false,
                "metadata" => [
                    "user_id" => Auth::user()->id,
                    "user_email" => Auth::user()->email,
                    'product' => $product->slug,
                    'product_name' => $product->name,
                    'subscription' => $product->subscription
                ]
            ));

            $customer_id = (isset($customer)) ? $customer->id : 0;

            return [
                'customer_id' => $customer_id,
                'charge_id' => $charge->id
            ];
        }
        catch(\Stripe\Error\Card $e) {
            return false;

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return false;

            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return false;

            // invalid card id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return false;

            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return false;

            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return false;

            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return false;

            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }

        return [
            'customer_id' => $customer_id,
            'charge_id' => $charge->id
        ];
    }
}