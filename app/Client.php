<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

use App\Traits\StatusVisibility;
use App\Traits\StatusActivity;
use App\Traits\StripeAPIable;
use App\Traits\Purchaseable;


class Client extends Model
{
    use StatusVisibility, StatusActivity, StripeAPIable, Purchaseable;



    protected $table = "clients";
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $fillable = [
        'agency_id',
        'created_by',
        'slug',
        'name',
        'phone',
        'website_url',
        'country',
        'subdomain',
        'hide',
        'disabled',
    ];

    

    public function members() 
    {
        return $this->belongsToMany('App\User')->withPivot('position')->withTimestamps();
    }

    // public function agency()
    // {
    //     return $this->belongsTo('App\Agency');
    // }

    public function memberInvites() 
    {
        return $this->hasMany('App\ClientMemberInvite');
    }

    // public function stripeCustomer() 
    // {
    //     return $this->morphOne('App\StripeCustomer', 'stripecustomerable');
    // }

    // public function purchases()
    // {
    //     return $this->hasMany('App\Purchase');
    // }

    // public function product_purchases()
    // {
    //     return $this->hasMany('App\Purchase')->with('product');
    // }





    public function scopeWhereSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }

    public function scopeWhereSubdomain($query, $subdomain) 
    {
        return $query->where('subdomain', '=', $subdomain);
    }
}