<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Your :attribute cannot be empty.',
            'first_name.max' => 'You\'re :attribute must not be more than 100 characters.',
            'last_name.required' => 'Your :attribute cannot be empty.',
            'last_name.max' => 'You\'re :attribute must not be more than 100 characters.',
        ];
    }
}
