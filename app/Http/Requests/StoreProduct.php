<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

// use Illuminate\Contracts\Validation\Validator;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:100|unique:products',
            'description' => 'max:250',
            // 'image' => 'image|mimes:jpeg,bmp,png,jpg|max:10000',
            's_amount' => 'sometimes|required|regex:/^\d*(\.\d{1,10})?$/',
            'v-price-name.*' => 'sometimes|required|max:30',
            'v-price-amount.*' => 'sometimes|required|regex:/^\d*(\.\d{1,10})?$/',
        ];

        // if ($this->validationData()['pricing_type'] == 'singular') {

        //     $rules = ['s_amount' => "required|regex:/^\d*(\.\d{1,2})?$/"];

        // }
        // else {
        //     $rules = ['v-price-name.*' => 'required|max:30'];
        // }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter the :attribute of the product.', 
            'name.max' => 'The :attribute must not be more than 100 characters.',
            'description.max' => 'The :attribute must not be more than 250 characters.',
            // 'image.image' => 'Image should be a valid image file.',
            // 'image.max' => 'Image should not be greater than 10MB size.',
            's_amount.required' => 'Please enter the price of the product.',
            's_amount.regex' => 'The price should be a valid price format.',
            'v-price-name.*.required' => 'All price name cannot be empty.',
            'v-price-name.*.max' => 'All price name must not be more than 30 characters.',
            'v-price-amount.*.required' => 'All price amount cannot be empty.',
            'v-price-amount.*.regex' => 'All price amount should be a valid price format.',
        ];
    }

    // protected function formatErrors(Validator $validator)
    // {
    //     return $validator->errors()->unique();
    // }
}
