<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAgencyInviteClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required','email', 'max:200', 'unique:clientinvites'],
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Please enter recipient\'s :attribute.', 
            'email.email'  => 'Looks like that is not a valida email format.',
            'email.max'  => 'Email must NOT have more than 200 characters.',
            'email.unique'  => 'That :attribute is already invited.',
        ];
    }
}
