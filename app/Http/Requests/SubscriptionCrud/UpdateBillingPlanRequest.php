<?php

namespace App\Http\Requests\SubscriptionCrud;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBillingPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nickname' => 'required|max:50',
            'trial_period_days' => 'numeric|min:0|max:720',
        ];
    }
    
    public function messages()
    {
        return [
            'nickname.required' => 'Please enter the :attribute of the product.', 
            'nickname.max' => 'The :attribute must not be more than 50 characters.',
            
            'trial_period_days.numeric' => 'The trial period must be a number.',
            'trial_period_days.min' => 'The trial period must not be a negative nunmber.',
            'trial_period_days.max' => 'The trial period must not be more than 720.',
        ];
    }
}
