<?php

namespace App\Http\Requests\ProductCrud;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePricingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_pricing_amount' => 'sometimes|required|regex:/^\d*(\.\d{1,10})?$/',
            's_pricing_period' => 'sometimes|required|min:1|integer',

            'v_pricing_name.*' => 'sometimes|required|max:30',
            'v_pricing_amount.*' => 'sometimes|required|regex:/^\d*(\.\d{1,10})?$/',
            'v_pricing_period.*' => 'sometimes|required|min:1|integer',
        ];
    }

    public function messages()
    {
        return [
            's_pricing_amount.required' => 'Please enter the price of the product.',
            's_pricing_amount.regex' => 'The amount should be a valid price format.',
            's_pricing_period.required' => 'The number of month for subscription is required.',
            's_pricing_period.integer' => 'The number of month for subscription must be a positive number.',
            's_pricing_period.min' => 'The number of month for subscription must be at least one month',

            'v_pricing_name.*.required' => 'All price name cannot be empty.',
            'v_pricing_name.*.max' => 'All price name must not be more than 30 characters.',
            'v_pricing_amount.*.required' => 'All price amount cannot be empty.',
            'v_pricing_amount.*.regex' => 'All price amount should be a valid price format.',
            'v_pricing_period.*.required' => 'All number of month for subscription is required.',
            'v_pricing_period.*.integer' => 'All number of month for subscription must be a number.',
            'v_pricing_period.*.min' => 'All number of month for subscription must be at least one month',
        ];
    }
}
