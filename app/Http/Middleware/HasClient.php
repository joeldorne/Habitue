<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class HasClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!isset(Auth::user()->client[0]))
        {
            return redirect('/');
        }
        
        /**
         * Add a global variable $myGroup to view that will hold the user's client group
         */
        view()->composer('*', function($view) {
            $view->with('myGroup', Auth::user()->myGroup());
        });

        return $next($request);
    }
}
