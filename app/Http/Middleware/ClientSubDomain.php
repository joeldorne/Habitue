<?php

namespace App\Http\Middleware;

use Closure, Auth;

use App\User;

class ClientSubDomain
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next) 
  {
    $route = $request->route();
    $subdomain = $route->parameter('subdomain');
    $route->forgetParameter('subdomain');

    $currentSubdomain = $this->getCurrentSubDomain($request, $next);

    $userClientSubdomain = $this->getUserClientSubdomain();

    // exit('domain: '.$userClientSubdomain);

    // if subdomain does NOT EXISTS. REMOVE subdomain and redirect
    // if ( !$this->isSubdomainExist($currentSubdomain) ) {
    

    return $next($request);
  }

  protected function getCurrentSubDomain($request, $next) 
  {

    $hostname = $request->getHost();
    $pieces = array_reverse(explode('.', $hostname));
    $currentSubdomain = ( isset($pieces[2]) ) ? ($pieces[2]) : "";

    // $currentSubdomain = ( $currentSubdomain == 'www' ) ? '' : $currentSubdomain;

    return $currentSubdomain;
    
  }

  protected function isSubdomainExist($subdomain) 
  {

    // check if exist on agency
    // if ( \App\Agency::WhereSubdomain($subdomain)->exists() ) {
      
    //   return true;

    // }


    // check if exist on client
    if ( \App\Client::WhereSubdomain($subdomain)->exists() ) {
      
      return true;
      
    }

    return false;
  }

  protected function getUserClientSubdomain() 
  {

    return Auth::user()->client[0]->subdomain;

  }


  
  public function old($request, Closure $next) 
  {
    $route = $request->route();
    $subdomain = $route->parameter('subdomain');
    $route->forgetParameter('subdomain');
    
    // return $next($request);

    // if logged-in without subdomain => redirecto with subdomain


    // if logged-in with subdomain
      // if owned subdomain is equal current subdomain
        // continue request path

      // if not! continue to public agency path

    
    // if guest without subdomain => continue to expected static pages


    // if guest with subdomain => continue to public agency path 

    
    // session(['key1' => 'value1']);

    // return $next($request);


    $domain_tld = config('session.domain_tld');
    $domain_sld = config('session.domain_sld');

    $currentSubdomain = $this->getCurrentSubDomain($request, $next);

    // redirect to 'habitue.dev' if entered subdomain is 'www'
    if ($currentSubdomain == 'www') {

      $request->headers->set('host', $domain_tld.'.'.$domain_sld);

      return redirect($request->path());
    }



    // continue if not logged in
    if (!Auth::check()) {
      return $next($request);
    }



    $user = Auth::user();

    // abort if invalid user role_id
    if (!in_array($user->role_id, [1,2,3]))
      return abort(404);


    // if user is CLIENT
    if ($user->role_id == 3) 
    {

      // if NOT a MEMBER yet
      if ($user->client->count() < 1) {
        
        // if WITH a subdomain
        if ($currentSubdomain != '') {

          // if subdomain does NOT EXISTS. REMOVE subdomain and redirect
          if ( !$this->isSubdomainExist($currentSubdomain) ) {

            $request->headers->set('host', 'habitue.'.$domain_sld);
            
            return redirect($request->path());

          }

        }

      }

    }
    


    return $next($request);
    dd($user);
    

    if(!Auth::user()->has('client')) {
      return $next($request);
    }

    // continue if user does not belong to a group
    if (Auth::user()->client->count() < 1) {
      return $next($request);
    }

    $role = Auth::user()->role->name;

    if ($role == 'client') {



      $subdomain = Auth::user()->client[0]->subdomain;

      // continue if subdomain is not set
      if ( !$subdomain ) {
        return $next($request);
      }

      // exit('-'.$subdomain);

      if ($subdomain != $currentSubdomain) {
        // exit('here1');
        $request->headers->set('host', $subdomain.'.habitue.'.$domain_sld);
        // Session::set('subdomain','$subdomain');
        return redirect($request->path());

      }

    }



    return $next($request);
  }






  public function veryold($request, Closure $next) 
  {
    // ray's local subdomain bypass
    return $next($request);

  	$hostname = $request->getHost();
  	$pieces = array_reverse(explode('.', $hostname));
  	
  	$currentSub = (isset($pieces[2]))?($pieces[2]):"";

    // detect if user is on subdomain
    $subdomain = Session::get('subdomain');

    // if not current signin and with currentSub
    // bypass subdomain and redirect
    if (!Auth::check() && $currentSub != "") {
      Session::set('subdomain','');
      $request->headers->set('host', 'kodily.com');
      return redirect($request->path());
    }

    // if not current signin and no currentSub
    // bypass subdomain and continue
    if (!Auth::check() && $currentSub == "") {
      Session::set('subdomain','');
      return $next($request);
    }

    // if user is an admin but the currentSub is not admin
    // set subdomain into admin and redirect
    if (Auth::user()->type == 1 && $currentSub != 'admin') {
      $request->headers->set('host', 'admin.kodily.com');
      Session::set('subdomain','');
      return redirect($request->path());
    }

    // if user is an admin and the currentSub is admin
    // continue
    if (Auth::user()->type == 1 && $currentSub == 'admin') {
      Session::set('subdomain','');
      return $next($request);
    }

    // if user is a freelancer but the currentSub is not freelancer
    // set subdomain into freelancer and redirect
    if (Auth::user()->type == 3 && $currentSub != 'freelancer') {
      $request->headers->set('host', 'freelancer.kodily.com');
      Session::set('subdomain','');
      return redirect($request->path());
    }

    // if user is a freelancer and the currentSub is freelancer
    // continue
    if (Auth::user()->type == 3 && $currentSub == 'freelancer') {
      Session::set('subdomain','');
      return $next($request);
    }

    ///////////////////////////////////// Test
    // $currentSub = "uoc2x";
    // if ($currentSub != "") {
    //   echo "<p>current: $currentSub</p>";
    //   $user = Auth::user();
    //   // $user_status = $user->status()->first();
    //   $currentSub_company = $user->companies()->where('public_url',$currentSub)->first();
    //   if (!$currentSub_company) {
    //     $request->headers->set('host', 'kodily.com');
    //     Session::set('subdomain','');
    //     return redirect()->route('error.505');
    //   }
    // }
    // exit("currentsub does not exist: -$currentSub-");
    /////////////////////////////////////

    // if current user is not client type
    // continue
    if (Auth::user()->type != 2) {
      return $next($request);
    }

    $user = Auth::user();
    $user_status = $user->status()->first();
    $company = $user->companies()->find($user_status->last_selected_company_id)->first();
    $current_selected_company_public_url = $company->public_url;

    // if subdomain is not triggered
    // if currentSub is not match with user's current selected company
    // change subdomain and redirect
    if ($subdomain == "" && $currentSub != $current_selected_company_public_url) {
      $request->headers->set('host', $current_selected_company_public_url.'.kodily.com');
      return redirect($request->path());
    }

    // if subdomain have been set && current-subdomain does not exist
    // commonly triggered on signin
    if($subdomain!="" && $currentSub==""){
      $request->headers->set('host', $subdomain.'.kodily.com');
      Session::set('subdomain','');
      return redirect($request->path());
    }

    // if subdomain does is not set && current-subdomain exist
    // cant figgure out what triggered this one
    // if($subdomain=="" && $currentSub!=""){
    //   $request->headers->set('host', 'kodily.com');
    //   return redirect($request->path());
    // }

    // if subdomain is set && current-subdomain is also set && subdomain is not equals to current-subdomain
    // commonly triggered when company is switched
    if($subdomain!="" && $currentSub!="" && $subdomain!=$currentSub){
      $request->headers->set('host', $subdomain.'.kodily.com');
      Session::set('subdomain','');
      return redirect($request->path());
    }

    Session::set('subdomain','');

    return $next($request);
  }
}