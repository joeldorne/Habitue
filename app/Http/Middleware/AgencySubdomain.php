<?php

namespace App\Http\Middleware;

use Closure, Auth;

class AgencySubdomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = $request->route();
        $subdomain = $route->parameter('subdomain');
        $route->forgetParameter('subdomain');
    
        $currentSubdomain = $this->getCurrentSubDomain($request, $next);
    
        $userClientSubdomain = $this->getUserClientSubdomain();
    
        // exit('domain: '.$userClientSubdomain);
    
        // if subdomain does NOT EXISTS. REMOVE subdomain and redirect
        // if ( !$this->isSubdomainExist($currentSubdomain) ) {
        
    
        return $next($request);
    }

    protected function getCurrentSubDomain($request, $next) 
    {
  
        $hostname = $request->getHost();
        $pieces = array_reverse(explode('.', $hostname));
        $currentSubdomain = ( isset($pieces[2]) ) ? ($pieces[2]) : "";

        // $currentSubdomain = ( $currentSubdomain == 'www' ) ? '' : $currentSubdomain;

        return $currentSubdomain;

    }

    protected function isSubdomainExist($subdomain) 
    {
  
      // check if exist on agency
      if ( \App\Agency::WhereSubdomain($subdomain)->exists() ) {
        
        return true;
  
      }
  
      return false;

    }

    protected function getUserClientSubdomain() 
    {
  
      return Auth::user()->agency()->first()->subdomain;
  
    }
}
