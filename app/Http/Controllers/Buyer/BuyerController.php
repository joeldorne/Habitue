<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;


class BuyerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'must-be-a-buyer-user']);
    }


    public function index()
    {
        return redirect()->route('buyer.dashboard');
    }


    public function dashboard()
    {
        return view('buyerpages.dashboard');
    }
}
