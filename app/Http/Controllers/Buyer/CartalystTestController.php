<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Cartalyst\Stripe\Stripe;

use Auth;
use App\User;
use App\ProductService;
use App\Subscription;
use Carbon\Carbon;


class CartalystTestController extends Controller
{
    protected $user, $stripe;

    public function __construct()
    {
        $this->user = User::where('email', 'user.two@habitue.com')->first();
        $this->stripe = new Stripe(config('services.stripe.secret'), config('services.stripe.version'));
        
        // dd($this->user);
        // dd($this->stripe);
    }

    public function customer()
    {
        // get a customer by id

        dd(
            $this->stripe->customers()->find($this->user->stripe_id)
        );
    }

    public function subscriptions()
    {
        // get all subscription from a customer

        dd(
            $this->stripe->subscriptions()->all($this->user->stripe_id)
        );

        // DELETE ALL SUBSCRIPTION FROM CUSTOMER ID
        $subscriptions = $this->stripe->subscriptions()->all($this->user->stripe_id);
        foreach ($subscriptions['data'] as $subs)
        {
            $this->stripe->subscriptions()->cancel($this->user->stripe_id, $subs['id']);
        }
    }

    public function subscribe()
    {
        // Given value
        $customer_id = $this->user->stripe_id;
        $plan_id = 'plan_DY95yDytd9B1AC';

        // Retrieve plan data
        $plan = $this->stripe->plans()->find($plan_id);

        // dd( $plan );

        $subscription = $this->stripe->subscriptions()->create($customer_id, [
            'plan' => $plan_id,
            'trial_end' => $this->parseTrialEndTimestamp($plan['trial_period_days'])
        ]);

        dd( $subscription );
    }

    protected function parseTrialEndTimestamp($days)
    {
        if (! $days) return null;

        return Carbon::now(config('app.timezone'))->addDays($days)->timestamp;
    }
}
