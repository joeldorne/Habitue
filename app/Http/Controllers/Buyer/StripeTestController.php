<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\ProductService;
use App\Subscription;

use Stripe\Customer as StripeCustomer;
use Stripe\Product as StripeProduct;
use Stripe\Plan as StripePlan;
use Stripe\Subscription as StripeSubscription;

use Laravel\Cashier\Cashier;


class StripeTestController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = User::where('email', 'user.two@habitue.com')->first();

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function customer()
    {
        $customer_id = $this->user->stripe_id;

        $customer = StripeCustomer::retrieve($customer_id);

        dd($customer);
    }

    public function subscriptions()
    {
        $customer_id = $this->user->stripe_id;

        $subscriptions = StripeSubscription::all([
            'customer' => $customer_id
        ]);

        dd($subscriptions);
    }

    public function cancel_subscriptions()
    {
        $subscriptions = $this->user->subscriptions;

        // dd($subscriptions);

        foreach($subscriptions as $subs) {
            $subs->cancel();
        }

        $subscriptions = $this->user->subscriptions;

        dd($subscriptions);

        // CANCEL DIRECTLY FROM STRIPE SERVER
        // $customer_id = $this->user->stripe_id;

        // $subscriptions = StripeSubscription::all([
        //     'customer' => $customer_id
        // ]);

        // foreach($subscriptions->data as $sub) {
        //     $sub->cancel();
        // }

        // $subscriptions = StripeSubscription::all([
        //     'customer' => $customer_id
        // ]);

        // dd($subscriptions);
    }

    public function checkout()
    {
        $customer_id = $this->user->stripe_id;
        $product_id = 'prod_DTYi0dQEV4BBmT';
        $plan_id = 'plan_DY95yDytd9B1AC';
        // $plan_id = 'plan_DTYjuXV4QCasOX';

        // set failed if currently still subscribed to this product
        if ($this->user->subscribed($product_id)){
            return 'already subscribed!';
        }
        else {
            return 'not subscribed';
        }


    }

    public function subscribe()
    {
        $customer_id = $this->user->stripe_id;
        $product_id = 'prod_DTYi0dQEV4BBmT';
        $plan_id = 'plan_DY95yDytd9B1AC';
        // $plan_id = 'plan_DTYjuXV4QCasOX';

        // set failed if currently still subscribed to this product
        if ($this->user->subscribed($product_id)){
            return 'already subscribed!';
        }
        else {
            return 'not subscribed';
        }

        // check if has ended subscribtion for this product
        // if yes, re-activate this subscription 
        // or delete the record to avoid duplication of records that will end up with messy scripts
        // if () {}
        // $user->subscription($product_id)->resume();

        $plan = $this->getStripePlan($plan_id);
        
        if ($plan->trial_period_days){
            $subscription = $this->user->newSubscription($product_id, $plan_id)->trialDays($plan->trial_period_days)->create();
        }
        else {
            $subscription = $this->user->newSubscription($product_id, $plan_id)->create();
        }

        dd($subscription);
    }

    public function getSubscription()
    {
        $product_id = 'prod_DTYi0dQEV4BBmT';

        $subscription = $this->user->subscription($product_id);

        dd($subscription);
    }

    protected function removeExpiredSubscriptionOf($user_id, $product_id)
    {
        //
    }

    protected function getStripeProduct($product_id)
    {
        return StripeProduct::retrieve($product_id);
    }

    protected function getStripePlan($plan_id)
    {
        return StripePlan::retrieve($plan_id);
    }
}
