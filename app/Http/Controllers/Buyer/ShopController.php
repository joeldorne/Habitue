<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Product;
use App\ProductService as Subscription;
use App\Purchase;

use Laravel\Cashier\Cashier;


class ShopController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'must-be-a-buyer-user']);
    }


    public function index()
    {
        return view('buyerpages.shop.index');
    }

    public function products()
    {
        $products = Product::Valid()->orderByDesc('id')->simplePaginate(20);

        return view('buyerpages.shop.products', compact('products'));
    }

    public function subscriptions()
    {
        // $subscriptions = Subscription::Active()->orderByDesc('id')->simplePaginate(20);
        $subscriptions = Subscription::orderByDesc('id')->simplePaginate(20);

        return view('buyerpages.shop.subscriptions', compact('subscriptions'));
    }

    public function subscribing(Subscription $subscription)
    {
        $user = Auth::user();

        /**
         * PENDING: Check if user is currently already have ANY subscriptions
         */

        /**
         * Check if user is currently already subsribe to this subscription
         */
        if ($user->subscribed($subscription->name))
        {
            $status = 'subscribed';

            return view('buyerpages.shop.subscribing', compact('subscription', 'status'));
        }

        $stripe_ps = $this->retrieveStripeProductService($subscription->stripe_product_id);

        $subscription->stripe_ps = $stripe_ps;

        return view('buyerpages.shop.subscribing', compact('subscription'));
    }

    public function purchasing(Product $product)
    {
        return view('buyerpages.shop.purchasing', compact('product'));
    }


    public function purchased(Request $request)
    {
        $product = Product::Slug($request->input('product_slug'))->firstOrFail();


        /**
         * Abort if received an invalid product price
         */
        if ( !$this->veriFySelectedPrice($product, $request->input('price_name'), $request->input('price_value')) )
        {
            return abort(404, 'Invalid Data');
        }
        

        /**
         * create stripe customer to the user if non-exist
         */
        $this->createStripeCustomerIfNotExist($request->input('stripeToken'));

        /**
         * convert price into cents
         */
        $price_by_cent = $request->input('price_value') * 100;
        
        /**
         * Stripe API charge
         */
        $charge = Auth::user()->charge($price_by_cent, [
            'receipt_email' => Auth::user()->email,
            'metadata' => [
                'product_id' => $product->id,
                'product_name' => $product->name,
                'product_desc' => $product->description,
                'buyer_id' => Auth::user()->id,
                'buyer_name' => Auth::user()->fullname,
                'buyer_email' => Auth::user()->email
            ]
        ]);


        /**
         * Store Purchase record to database
         */
        if ( $charge->status == 'succeeded')
        {
            Auth::user()->mypurchases()->create([
                'slug' => Purchase::generateSlug(),
                'product_id' => $product->id,
                'name' => $product->name,
                'price' => $request->input('price_value'),
                'activate_form_url' => $product->activate_form_url,
                'stripe_id' => Auth::user()->stripe_id,
                'charge_id' => $charge->id
            ]);
        }


        return redirect()->route('buyer.purchases');
    }

    protected function veriFySelectedPrice(Product $product, $price_name='', $price_value='') : bool
    {
        if ( $product->pricing_type == 'singular' )
        {
            return $product->singular_price == $price_value;
        }
        else if ( $product->pricing_type == 'variation' )
        {
            $arr_prices = $product->variation_prices_as_array();

            foreach ($arr_prices as $obj_price)
            {
                if ( $obj_price->name == trim($price_name) && $obj_price->amount == trim($price_value) )
                {
                    return true;
                }
            }
        }

        return false;
    }

    protected function createStripeCustomerIfNotExist($token)
    {
        if (Auth::user()->hasStripeId())
        {
            return true;
        }


        Auth::user()->createAsStripeCustomer($token);
    }

    protected function retrieveStripeProductService($stripe_product_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_product = \Stripe\Product::retrieve($stripe_product_id);

        $stripe_product->plans = $this->retrieveStripeBillingPlanByProductId($stripe_product->id);
        
        return $stripe_product;
    }

    protected function retrieveStripeBillingPlanByProductId($stripe_ps_id)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_plans = \Stripe\Plan::all(array(
            'active' => true,
            'product' => $stripe_ps_id,
        ));
        
        return $stripe_plans;
    }
}
