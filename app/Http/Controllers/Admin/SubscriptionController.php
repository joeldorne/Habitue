<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\ProductService as Subscription;

use App\Repositories\Subscription\Contracts\SubscriptionRepositoryInterface;

use App\Http\Requests\SubscriptionCrud\StoreBasicRequest;
use App\Http\Requests\SubscriptionCrud\UpdateBasicRequest;
use App\Http\Requests\SubscriptionCrud\UpdateImageRequest;
use App\Http\Requests\SubscriptionCrud\StoreBillingPlanRequest;
use App\Http\Requests\SubscriptionCrud\UpdateBillingPlanRequest;


class SubscriptionController extends Controller
{
    protected $subscriptionRepo;

    public function __construct(SubscriptionRepositoryInterface $subscriptionRepo)
    {
        $this->middleware(['auth','admin']);

        $this->subscriptionRepo = $subscriptionRepo;
    }

    public function list()
    {
        $subscriptions = Subscription::orderByDesc('id')->simplePaginate(1120);

        return view('adminpages.subscriptions.list', compact('subscriptions'));
    }


    public function view(Subscription $product_service)
    {
        return view('adminpages.subscriptions.view', compact('product_service'));
    }

    public function create()
    {
        return view('adminpages.subscriptions.create');
    }

    public function edit(Subscription $product_service)
    {
        return view('adminpages.subscriptions.edit', compact('product_service'));
    }

    /**
     * Store newly create subscription and create new Stripe-Product-Service
     * 
     * @param StoreBasicRequest $request
     * @return response
     */
    public function storeBasic(StoreBasicRequest $request)
    {
        // create an unsaved model for new subscription 
        $subscription_model = $this->subscriptionRepo->new($request->only(['name', 'description']));

        // save the new subscription model via User relational method
        $new_subscription= Auth::user()->product_services()->save($subscription_model);

        // get the newly created subscription
        $subscription = $this->subscriptionRepo->find($new_subscription->id);

        // save the new subsctiption to stripe account and retrieve the data
        $stripe_service = $this->create_new_stripe_service($subscription);

        // add the stripe product id to the product-subscription database
        $subscription->stripe_product_id = $stripe_service->id;
        $subscription->save();

        $response = ['status'=>'success', 'subscription'=>$subscription];

        return response()->json($response, 201);
    }

    /**
     * Create a new Stripe Product-Service
     * 
     * @param Subscription $subscription
     * @return \Stripe\Product $stripe_service
     */
    protected function create_new_stripe_service(Subscription $subscription) : \Stripe\Product
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripe_service = \Stripe\Product::create([
            'name' => $subscription->name,
            'type' => 'service',
            'active' => 'false',
            'metadata' => [
                'product_service_id' => $subscription->id,
                'product_service_slug' => $subscription->slug,
                'product_url' => 'to be included',
                'product_desc' => $subscription->description
            ],
        ]);

        return $stripe_service;
    }

    public function updateBasic(UpdateBasicRequest $request)
    {
        $update_stripe = false;

        // get a copy of subscription before update to assert if field[name] is change
        $subscription = $this->subscriptionRepo->find($request->input('id'));

        // flag to update the stripe data if name is changed
        if ($subscription->name != $request->input('name') )
        {
            $update_stripe = true;
        }

        // update the subscription
        $updated = $this->subscriptionRepo->update($request->only(['name', 'description']), $request->input('id'));

        $subscription = $this->subscriptionRepo->find($request->input('id'));

        // update the stripe data
        if ($updated && $update_stripe)
        {
            $this->update_existing_stripe_service_name($subscription);
        }

        $response = ['status'=>'success', 'subscription'=>$subscription];

        return response()->json($response, 201);
    }

    /**
     * Update the name of the Stripe Product-Service
     */
    protected function update_existing_stripe_service_name(Subscription $subscription)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        \Stripe\Product::update(
            $subscription->stripe_product_id,
            array('name' => $subscription->name)
        );
    }

    public function updateImage(UpdateImageRequest $request)
    {
        $subscription = $this->subscriptionRepo->findWhereSlug($request->input('slug'));

        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $image_path = $this->uploadPhysicalImage($request, 'subscriptions/images');

            // update product database
            $subscription->image = $image_path;
            $subscription->save();
            
            // prepare and return response
            $response = ['status'=>'success', 'subscription'=>$subscription];

            return response()->json($response, 201);
        }
        else 
        {
            $response = ['status'=>'failed', 'message'=>'Unknown error'];

            return response()->json($response, 422);
        }
    }

    /**
     * Upload Image to the designated storage. Subscription storage
     * 
     * @param UpdateImageRequest $request
     * @param string $store_path. path on where to store the image
     * @return string $path. Image file stored path
     */
    protected function uploadPhysicalImage(UpdateImageRequest $request, $store_path) : string
    {
        $path = 'storage/'.$store_path;

        if(! \File::isDirectory($path)) {

            \Storage::makeDirectory('public/'.$store_path);
        }

        // check if previous image is not default and exist. delete if all true.

        // prepare image filename
        $extension = $request->image->getClientOriginalExtension();
        $str_random = str_random(20);
        $filename = $str_random.'.'.$extension;

        // store image to storage
        $request->image->storeAs($store_path, $filename, 'public');

        return $path.'/'.$filename;
    }

    /**
     * Fetch all Billing Plans from the given Stripe Product-Service
     * 
     * @param Request $request
     * @return response
     */
    public function fetchPlans(Request $request)
    {
        $subscription = $this->subscriptionRepo->findWhereSlug($request->input('slug'));


        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        try {
            $list_stripe_plan = \Stripe\Plan::all([
                'product' => $subscription->stripe_product_id,
            ]);

            $response = [
                'status' => 'success',
                'success' => true,
                'plans' => $list_stripe_plan
            ];

            return response()->json($response, 201);
        }
        catch(\Exception $e) {
            $response = [
                'status' => 'failed',
                'failed' => true,
                'errors' => [
                    '422' => [
                        'Sorry we are having trouble connecting to the server! If this issue persist please refresh the whole page.'
                    ],
                ],
            ];

            return response()->json($response, 422);
        }
    }

    /**
     * Create a Billing Plan for the given Stripe-Plan
     * 
     * @param StoreBillingPlanRequest $request
     * @return response
     */
    public function storePlan(StoreBillingPlanRequest $request)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        try {
            $plan = \Stripe\Plan::create([
                'currency' => $request->input('currency'),
                'interval' => $request->input('interval'),
                'interval_count' => $request->input('interval_count'),
                'product' => $request->input('stripe_product_id'),
                'nickname' => $request->input('nickname'),
                'amount' => $request->input('amount'),
                'active' => $request->input('active'),
                'trial_period_days' => $request->input('trial_period_days'),
            ]);

            $response = [
                'status' => 'success',
                'success' => true,
                'plan' => $plan
            ];

            return response()->json($response, 201);
        }
        catch(\Exception $e) {
            $response = [
                'status' => 'failed',
                'failed' => true,
                'errors' => [
                    '422' => [
                        'Sorry we are having trouble connecting to the server! If this issue persist please refresh the whole page.'
                    ],
                ],
            ];

            return response()->json($response, 422);
        }
    }

    /**
     * Update a Billing Plan for the given Stripe-Plan
     * 
     * @param UpdateBillingPlanRequest $request
     * @return response
     */
    public function updatePlan(UpdateBillingPlanRequest $request)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        try {
            $plan = \Stripe\Plan::update($request->input('stripe_plan_id'), array(
                'nickname' => $request->input('nickname'),
                'active' => $request->input('active'),
                'trial_period_days' => $request->input('trial_period_days'),
            ));

            $response = [
                'status' => 'success',
                'success' => true,
                'plan' => $plan
            ];

            return response()->json($response, 201);
        }
        catch(\Exception $e) {
            $response = [
                'status' => 'failed',
                'failed' => true,
                'errors' => [
                    '422' => [
                        'Sorry we are having trouble connecting to the server! If this issue persist please refresh the whole page.'
                    ],
                ],
            ];

            return response()->json($response, 422);
        }
    }

    /**
     * Remove a Billing Plan for the given Stripe-Product-Plan
     * 
     * @param Request $request
     * @return response
     */
    public function removePlan(Request $request)
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        try {
            $plan = \Stripe\Plan::retrieve($request->input('stripe_plan_id'));
            $plan->delete();

            $response = [
                'status' => 'success',
                'success' => true,
                'plan' => $plan
            ];

            return response()->json($response, 200);
        }
        catch(\Exception $e) {
            $response = [
                'status' => 'failed',
                'failed' => true,
                'errors' => [
                    '422' => [
                        'Unable to find this Billing Plan. Please refresh the page.'
                    ],
                ],
            ];

            return response()->json($response, 422);
        }
    }

    /**
     * Update the Product Visiblity including the Stripe-Product-Service active attribute
     * 
     * @param Request $request
     * @return response
     */
    public function updateVisibility(Request $request)
    {
        $active = $request->input('disabled') == 'true' ? true : false;
        $disabled = $request->input('disabled') == 'true' ? 0 : 1;
        $slug = $request->input('slug');


        // Get Subscription
        try {
            $subscription = $this->subscriptionRepo->findWhereSlug($slug);
        }
        catch(\Exception $e) {
            $response = [
                'status' => 'failed',
                'failed' => true,
                'errors' => [
                    '422' => [
                        'Strange! This subscription does not exists anymore. Please refresh the page.'
                    ],
                ],
            ];

            return response()->json($response, 422);
        }


        // Update Stripe Product-Service
        try {
            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

            $stripe_product_service = \Stripe\Product::update($subscription->stripe_product_id, array(
                'active' => $active
            ));
        }
        catch (\Excecption $e) {
            $response = [
                'status' => 'failed',
                'failed' => true,
                'errors' => [
                    '422' => [
                        'Strange! Unable to find the stripe data for this subscription. Please refresh the page and try again.'
                    ],
                ],
            ];

            return response()->json($response, 422);
        }

        
        // Update the subscription's local database 
        $subscription->disabled = $disabled;
        $subscription->save();
        
        $response = [
            'status' => 'success', 
            'success' => true, 
            'subscription' => $subscription];
        
        return response()->json($response, 201);
    }
}
