<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;

use App\Product;

use App\Repositories\Product\Contracts\ProductRepositoryInterface;

use App\Http\Requests\ProductCrud\StoreBasicRequest;
use App\Http\Requests\ProductCrud\UpdateBasicRequest;
use App\Http\Requests\ProductCrud\UpdateImageRequest;
use App\Http\Requests\ProductCrud\UpdatePricingRequest;
use App\Http\Requests\ProductCrud\UpdateActivationFormUrlRequest;


class ProductController extends Controller
{
    protected $productRepo;

    public function __construct(ProductRepositoryInterface $productRepo)
    {
        $this->middleware(['auth']);

        $this->productRepo = $productRepo;
    }


    public function list()
    {
        $products = Product::with('seller')->get();
        
        return view('adminpages.products.list', compact('products'));
    }


    public function create()
    {
        return view('adminpages.products.create');
    }


    public function view(Product $product)
    {
        return view('adminpages.products.view', compact('product'));
    }


    public function edit(Product $product)
    {
        return view('adminpages.products.edit', compact('product'));
    }


    public function storeBasic(StoreBasicRequest $request)
    {
        $product_model = $this->productRepo->new($request->only(['name', 'description']));

        $new_product = Auth::user()->myproducts()->save($product_model);

        $product = $this->productRepo->find($new_product->id);

        $response = ['status'=>'success', 'product'=>$product];

        return response()->json($response, 201);
    }


    public function updateBasic(UpdateBasicRequest $request)
    {
        $updated = $this->productRepo->update($request->only(['name', 'description']), $request->input('id'));

        $product = $this->productRepo->find($request->input('id'));

        $response = ['status'=>'success', 'product'=>$product];

        return response()->json($response, 201);
    }

    public function updateImage(UpdateImageRequest $request)
    {
        $product = $this->productRepo->findWhereSlug($request->input('slug'));

        if ($request->hasFile('image') && $request->file('image')->isValid())
        {
            $path = 'storage/products/images/';
            if(! \File::isDirectory($path)) {
    
                \Storage::makeDirectory('public/products/images');
            }

            // check if previous image is not default and exist. delete if all true.

            // prepare image filename
            $extension = $request->image->getClientOriginalExtension();
            $str_random = str_random(20);
            $filename = $str_random.'.'.$extension;

            // store image to storage
            $request->image->storeAs('products/images', $filename, 'public');

            // update product database
            $product->image = $path.$filename;
            $product->save();

            // prepare and return response
            $response = ['status'=>'success', 'product'=>$product];

            return response()->json($response, 201);
        }
        else 
        {
            $response = ['status'=>'failed', 'message'=>'Unknown error'];

            return response()->json($response, 422);
        }
    }

    public function updatePaymentMethod(Request $request)
    {
        $allowed_methods = ['one time pay', 'subscription'];

        $payment_method = strtolower($request->input('payment_method'));
        $slug = $request->input('slug');


        // check if entered payment_method is valid data
        if (!in_array($payment_method, $allowed_methods))
        {
            $response = ['status'=>'failed', 'message'=>'Invalid Payment Method received!'];

            return response()->json($response, 422);
        }


        $updated = $this->productRepo->updateWhereSlug(['payment_method'=>$payment_method], $slug);

        $product = $this->productRepo->findWhereSlug($slug);
        

        $response = ['status'=>'success', 'product'=>$product];
        
        return response()->json($response, 201);
    }

    public function updatePricingSingular(UpdatePricingRequest $request)
    {
        $slug = $request->input('slug');

        /**
         * init the data to be updated
         * if the product is subscription insert the period to the data
         */
        $data = [];
        $data['pricing_type'] = 'singular';
        $data['singular_price'] = $request->input('s_pricing_amount');
        if ( $request->has('s_pricing_period') ) {
            $data['singular_subscription_period'] = $request->input('s_pricing_period');
        }
        $this->productRepo->updateWhereSlug($data, $slug);

        $product = $this->productRepo->findWhereSlug($slug);
        

        $response = ['status'=>'success', 'product'=>$product];
        
        return response()->json($response, 201);
    }

    public function updatePricingVariation(UpdatePricingRequest $request)
    {
        $slug = $request->input('slug');

        // fetch data
        $name = $request->input('v_pricing_name');
        $amount = $request->input('v_pricing_amount');
        $default = $request->input('v_pricing_default');
        if ($request->has('v_pricing_period')) {
            $period = $request->input('v_pricing_period');
        }

        // prepare data for compression
        if ($request->has('v_pricing_period')) {
            $compact = compact('name', 'amount', 'period', 'default');
        }
        else {
            $compact = compact('name', 'amount', 'default');
        }

        // compress the data into: array[{object}, {object}, {object}, ...]
        $variation_prices = $this->compressVariationPrices($compact);

        $data = [];
        $data['pricing_type'] = 'variation';
        $data['variation_prices'] = json_encode($variation_prices);

        $this->productRepo->updateWhereSlug($data, $slug);

        $product = $this->productRepo->findWhereSlug($slug);


        $response = ['status'=>'success', 'product'=>$product];

        return response()->json($response, 201);
    }
    
    protected function compressVariationPrices($data) 
    {
        $array_of_objects = [];

        for ($i=0; $i<count($data['name']); $i++)
        {
            $obj = [];

            foreach ($data as $k=>$v) {
                if ($k == 'amount') $v[$i] = number_format($v[$i], 2);
                if ($k == 'default') {
                    $v[$i] = $v[$i] == 'true'? true : false;
                }
                $obj[$k] = $v[$i];
            }

            array_push($array_of_objects, (object)$obj);
        }

        return $array_of_objects;
    }

    public function updateActivationFormUrl(UpdateActivationFormUrlRequest $request)
    {
        $slug = $request->input('slug');


        $this->productRepo->updateWhereSlug(['activate_form_url'=>$request->input('activate_form_url')], $slug);


        $product = $this->productRepo->findWhereSlug($slug);
        

        $response = ['status'=>'success', 'product'=>$product];
        
        return response()->json($response, 201);
    }

    public function updateVisibility(Request $request)
    {
        $disabled = strtolower($request->input('disabled'));
        $slug = $request->input('slug');


        $updated = $this->productRepo->updateWhereSlug(['disabled'=>$disabled], $slug);

        $product = $this->productRepo->findWhereSlug($slug);
        

        $response = ['status'=>'success', 'product'=>$product];
        
        return response()->json($response, 201);
    }
}