<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\Product\Contracts\ProductRepositoryInterface;

use Auth;
use App\ProductService;

// use Stripe\Customer as StripeCustomer;
use Stripe\Product as StripeProduct;
use Stripe\Plan as StripePlan;


class StripeTestController extends Controller
{
    protected $productRepo;

    public function __construct(ProductRepositoryInterface $productRepo)
    {
        $this->middleware(['auth']);

        $this->productRepo = $productRepo;
    }

    public function create()
    {
        //////////////////
        // $user = Auth::user();

        // $subscriptions = ProductService::find(20);

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));


        /**
         * CREATE SERVICES
         */
        // $stripe_product = StripeProduct::create([
        //     'name' => 'My SaaS Platform 5',
        //     'type' => 'service',
        //     'active' => 'false',
        //     'metadata' => [
        //         'key1' => 'data1',
        //         'key2' => 'data2',
        //         'key3' => 'data3',
        //     ],
        // ]);

        /**
         * CREATE PLAN FOR SERVICES
         */
        // $plan = StripePlan::create([
        //     'currency' => 'clp',
        //     'interval' => 'year', // day, week, month, year
        //     'interval_count' => 1,
        //     'product' => $stripe_product->id,
        //     'nickname' => 'Pro Plan',
        //     'amount' => 3000,
        //     'active' => true,
        //     'metadata' => [
        //         'key1' => 'data1',
        //         'key2' => 'data2',
        //         'key3' => 'data3',
        //     ],
        // ]);
        // dd($plan);


        /**
         * RETRIEVE SERVICE
         */
        // $stripe_product = StripeProduct::retrieve('prod_DS13J5wECxxyL6');
        // $stripe_product->name = 'My SaaS Platform 4x';
        // $stripe_product->save();
        // dd($stripe_product);

        /**
         * UPDATE SERVICE
         */
        // $stripe_product = StripeProduct::update('prod_CUY16kAd33Gtpx', array(
        //     'name' => 'My SaaS Platform 4xx'
        // ));
        // dd($stripe_product);

        /**
         * RETRIEVE AND UPDATE PLAN
         */
        // $stripe_plan = StripePlan::retrieve('plan_DS13JLK1jCFRMv');
        // $stripe_plan->active = true;
        // $stripe_plan->save();
        // dd($stripe_plan);

        /**
         * SEARCH AND LIST OF PLANS
         */
        // try {
        //     $list_stripe_plan = StripePlan::all([
        //         'limit' => 10,
        //         'active' => true,
        //         'product' => 'prod_DSQry8qBUndq7V',
        //     ]);
        //     dd($list_stripe_plan);
        // }
        // catch(\Exception $e) {
        //     echo 'error';
        // }

        // foreach ($list_stripe_plan->data as $plan)
        // {
        //     echo '<br>id: '.$plan->id;
        // }
    }
}