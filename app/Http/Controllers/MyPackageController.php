<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Package;
use App\MyPackage;

class MyPackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function mypackages() 
    {
        $mypackages = MyPackage::isActive()->isVisible()->get();

        return view('mypackages.mypackages', compact('mypackages'));
    }

    public function mypackage()
    {
        //
    }

    public function generateUniqueMyPackageSlug() 
    {
        do {
            $slug = 'mp_'.str_random(10);
        } while(MyPackage::Slug($slug)->exists());

        return $slug;
    }
}
