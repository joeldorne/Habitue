<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Package;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function packages() 
    {
        $packages = Package::isActive()->isHidden()->get();

        return view('packages.packages', compact('packages'));
    }
}
