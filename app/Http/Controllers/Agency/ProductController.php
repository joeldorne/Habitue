<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use App\Agency;
use App\Product;

use App\Http\Requests\StoreProduct;
use App\Http\Requests\UpdateProduct;
// use App\Http\Requests\StoreProductImage;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('agency.user');
        $this->middleware('has.agency');
    }

    public function products()
    {
        $products = Product::FromCurrentAgency()->get();

        return view('agencypages.products.products', compact('products'));
    }

    public function show($slug) 
    {

        // exit('product show page: '.$slug);
        $product = Product::WhereSlug($slug)->firstOrFail();
        dd($product);

    }

    public function create() 
    {
        return view('agencypages.products.create');
    }

    public function edit(Product $product)
    {
        return view('agencypages.products.edit', compact('product'));
    }

    public function store(StoreProduct $request) 
    {
        $data = $this->handleStoreData( $request );


        $product = Product::create($data);

        
        // handles non-cropper uploaded image file 
        // if ($request->hasFile('image') && $request->file('image')->isValid()) {

        //     $filename = $this->handleUploadedImage($request, $product);


        //     $product->image = $filename;

        //     $product->save();
        // }

        
        return [
            'status' => 'success', 
            'slug' => $product->slug,
            'message' => "Product successfully created! View the new <a href='/agency/product/".$product->slug."'>product</a>."
        ];
    }

    public function update(UpdateProduct $request) 
    {

        $data = $this->handleStoreData( $request );

        $product = Product::WhereSlug($request->slug)->update($data);


        return [
            'status' => 'success', 
            'slug' => $request->slug,
            'message' => "Product successfully updated! View the new <a href='/agency/product/".$request->slug."'>product</a>."
        ];

    }

    protected function handleStoreData($request) 
    {

        $data = [];


        $data['name'] = $request->name;

        $data['description'] = $request->description;


        if ($request->payment_method == 'subscription') {

            $data['subscription'] = 1;

        }
        else {

            $data['subscription'] = 0;

        }


        $data['price_type'] = $request->pricing_type;

        if ($request->pricing_type == 'singular') {

            $data['price'] = $request->s_amount;

        }
        else {

            $v_prices = $this->handleVPrices($request['v-price-name'], $request['v-price-amount'], $request['v-price-default']);

            $data['price_variations'] = json_encode($v_prices);
            
        }


        $data['disabled'] = ($request->status == 'active') ? 0 : 1;

        $data['external_form'] = $request->external_form;


        return $data;
    }

    protected function handleVPrices($v_prices_name, $v_prices_amount, $v_prices_default) 
    {
        $v_prices = [];

        foreach ($v_prices_name as $key => $name){

            array_push($v_prices, (object)[

                'name' => $name,
                'amount' => $v_prices_amount[$key],
                'default' => $v_prices_default[$key],

            ]);

        }

        return $v_prices;
    }

    public function storeImage(Request $request) 
    {

        if ($request->hasFile('image') && $request->file('image')->isValid() && $request->slug != '') 
        {

            $path = 'storage/products/images/';
            if(! \File::isDirectory($path)) {
    
                \Storage::makeDirectory('public/products/images');
    
            }

            $product = Product::WhereSlug($request->slug)->firstOrFail();

            if ( !$product ) return;

            
            $filename = $product->slug.'.jpeg';


            $request->image->storeAs('products/images', $filename, 'public');

    
            $product->image = $path.$filename;

            $product->save();
         

            return;
        }
    }

    public function setToActive(Product $product) 
    {

        $product->disabled = 0;
        $product->save();

        return redirect()->back();

    }

    public function setToInactive(Product $product) 
    {

        $product->disabled = 1;
        $product->save();

        return redirect()->back();

    }

    // NOT USED
    protected function handleUploadedImage(StoreProduct $request, Product $product) 
    {
        $path = 'storage/products/images/';
        if(! \File::isDirectory($path)) {

            \Storage::makeDirectory('public/products/images');

        }

        $filename = $product->slug.'.'.$request->image->getClientOriginalExtension();


        $request->image->storeAs('products/images', $filename, 'public');


        return $path.$filename;
    }
    

    // TO BE DELETED
    public function tempImageStore(Request $request) 
    {
        // return $request->temp_image;

        $path = 'storage/products/temps/';
        if(! \File::isDirectory($path)) {

            \Storage::makeDirectory('public/products/temps');

        }


        // $filename = str_random(30).'.'.$request->temp_image->getClientOriginalExtension();
        $filename = str_random(30).'.jpeg';


        $request->temp_image->storeAs('products/temps', $filename, 'public');


        return $filename;
    }


    // TO BE DELETED
    public function cropper() 
    {
        return view('agencypages.products.cropper');
    }


    // TO BE DELETED
    public function postcropper(Request $request) 
    {
        return $request;
        $filename = 'xxxx.png';
        $request->croppedImage->storeAs('products', $filename, 'public');
        return $request->croppedImage;
    }


    //TEMP
    public function uploadFileSample1(Request $reques)
    {
        $file = Input::file('file');
        $filename = $file->getClientOriginalName();

        $path = hash( 'sha256', time());

        if(Storage::disk('uploads')->put($path.'/'.$filename,  File::get($file))) {
            $input['filename'] = $filename;
            $input['mime'] = $file->getClientMimeType();
            $input['path'] = $path;
            $input['size'] = $file->getClientSize();
            $file = FileEntry::create($input);

            return response()->json([
                'success' => true,
                'id' => $file->id
            ], 200);
        }
        return response()->json([
            'success' => false
        ], 500);
    }


    /**
     * Sample upload file loop
     */
    public function storeUploadLoop(Request $request)
    {
        if($request->hasFile('files')) {
            collect($request->file('files'))->each(function(UploadedFile $file){   
              $this->validateFiles($file);
              $filesname = $file->store('folder-name', ['disk' => 'public']);
              
                // Will output: "folder-name/randomstring.jpg" (if you uploaded a jpeg)
               dd($filename);
            });
        }
    }
}
