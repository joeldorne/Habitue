<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;

use App\User;
use App\Agency;
use App\ClientInvite;

use App\Http\Requests\StoreAgencyInviteClient;

use App\Mail\AgencyInviteClientMail;


class AgencyClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('agency.user', ['except'=>['create','store']]);
        $this->middleware('has.agency', ['except'=>['create','store']]);
    }

    public function clients()
    {
        $invites = ClientInvite::FromAgency(Auth::user()->agency[0]->id)->get();
        
        return view('agencypages.clients.clients', compact('invites'));
    }

    public function invite()
    {
        return view('agencypages.clients.invite');
    }

    public function storeInvite(StoreAgencyInviteClient $request) 
    {
        $invite = ClientInvite::create([
            'invited_by' => Auth::user()->id,
            'agency_id' => Auth::user()->agency[0]->id,
            'contact_name' => $request->contact_name,
            'email' => $request->email,
            'institution_name' => $request->institution_name,
            'message' => $request->message,
        ]);
        
        // send mail
        Mail::to($invite)->send(new AgencyInviteClientMail($invite));

        return redirect()->route('agency.clients')->withSuccess('An email have been successfully sent to \''.$request->email.'\'.');
    }
}
