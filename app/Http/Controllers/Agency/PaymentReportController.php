<?php

namespace App\Http\Controllers\Agency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use App\Purchase;


class PaymentReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() 
    { 
        $products_sold = Auth::user()->myAgency()->product_sales->reverse()->slice(0, 10);

        return view('agencypages.paymentreports.index', compact('products_sold'));
    }


    public function purchases()
    {
        $products_sold = Auth::user()->myAgency()->product_sales->reverse();
        
        return view('agencypages.paymentreports.purchases', compact('products_sold'));
    }


    public function updateCompletion(Request $request) 
    {
        $purchase = Purchase::Slug($request->purchase_slug)->firstOrFail();

        if (! $purchase) {

            return [
                'status' => 'error',
                'message' => 'Invalid data. Please refresh the page and try again.',
            ];

        }

        $purchase->completion = $request->completion;
        $purchase->save();

        return [
            'status' => 'success',
            'message' => 'Updated.',
        ];
    }

    
    public function updateStatus(Request $request)
    {
        $purchase = Purchase::Slug($request->purchase_slug)->firstOrFail();

        if (! $purchase) {

            return [
                'status' => 'error',
                'message' => 'Invalid data. Please refresh the page and try again.',
            ];

        }

        $purchase->status = $request->status;
        $purchase->save();

        return [
            'status' => 'success',
            'message' => 'Updated.',
        ];
    }
}
