<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\FooRepository;

use App\Product;


class TestController extends Controller
{
    public function agencyMemberInviteMailable() 
    {
        $email = "email@email.com";
        $token = "7KNq5iDAAdGWfvnus8ryvoMGTLhw5RaITn3zfZtTdxPKe7YGpmpnqWO4dEzR";
        return view('mailables.agency-member-invite', compact('email', 'token'));
    }

    public function foo(FooRepository $foo) 
    {
        return $foo->get();
    }

    public function test4()
    {
        $products = Product::where('id', '<', 203)->get();


        foreach ($products as $p) {

            echo '<br>'.$this->getAmount($p->variation_prices);
        }
    }

    public function getAmount($variation_prices, $data = 'amount')
    {
        $json = json_decode($variation_prices);

        $html = '';

        foreach($json as &$price) {

            $html .= "$ ".$price->$data. ", ";
        }

        return $html;
    }
}
