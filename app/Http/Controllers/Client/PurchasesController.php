<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use App\Purchase;


class PurchasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('client.user');
        $this->middleware('has.client');
    }


    public function purchases() 
    {
        $purchases = Auth::user()->myClient()->product_purchases->reverse();

        return view('clientpages.purchases.purchases', compact('purchases'));
    }
}
