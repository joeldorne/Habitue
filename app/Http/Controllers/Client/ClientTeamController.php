<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth;

use App\User;
use App\Client;
use App\ClientMemberInvite;

use App\Http\Requests\StoreClientTeamInvite;

use App\Mail\ClientMemberInviteMail;


class ClientTeamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('client.user');
        $this->middleware('has.client');
    }

    public function team()
    {
        return view('clientpages.teams.members');
    }

    public function teamInviteStore(StoreClientTeamInvite $request)
    {
        $invite = ClientMemberInvite::create([
            'invited_by' => Auth::user()->id,
            'email' => $request->email,
            'client_id' => Auth::user()->client[0]->id,
        ]);
        
        //send mail
        Mail::to($invite)->send(new ClientMemberInviteMail($invite));

        return redirect()->back()->withSuccess('An email have been succesfully sent to invite the new member.');
    }
}
