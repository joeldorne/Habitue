<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\StripeCustomer;

class PaymentCardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function mycards() 
    {
        $customer = $this->getStripeCustomerObj();
        if ( !isset($customer->object) )
            return $this->stripeErrorRedirect('packages');

        return view('mycards.mycards', compact('customer'));
    }

    public function add() 
    {
        return view('mycards.add');
    }

    public function store(Request $request)
    {
        // dd($request);
        /*
         * Get Customer Object from Stripe server
         * Customer Object for the current user
         */
        $customer = $this->getStripeCustomerObj();
        if ( !isset($customer->object) )
            return $this->stripeErrorRedirect('packages');

        /*
         * add the card to the customer object
         */
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $token = $request->stripeToken;

        try {
            $customer->sources->create(array(
                "source" => $token,
                // "name" => Auth::user()->first_name.' '.Auth::user()->last_name,
            ));
        }
        catch(\Stripe\Error\Card $e) {
            return $this->stripeErrorRedirect('mycards.add');

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return $this->stripeErrorRedirect('mycards.add');

            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return $this->stripeErrorRedirect('mycards.add');

            // invalid card id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return $this->stripeErrorRedirect('mycards.add');

            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return $this->stripeErrorRedirect('mycards.add');

            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return $this->stripeErrorRedirect('mycards.add');

            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return $this->stripeErrorRedirect('mycards.add');

            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }
        
        return redirect()->route('mycards');
    }

    public function remove($card) 
    {
        $card_id = $card;
        /*
         * Get Customer Object from Stripe server
         * Customer Object for the current user
         */
        $customer = $this->getStripeCustomerObj();
        if ( !isset($customer->object) )
            return $this->stripeErrorRedirect('packages');

        try {
            $customer->sources->retrieve($card_id)->delete();
        }
        catch(\Stripe\Error\Card $e) {
            return $this->stripeErrorRedirect('mycards');

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return $this->stripeErrorRedirect('mycards');
            
            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return $this->stripeErrorRedirect('mycards');
            
            $this->stripeErrorRedirect();
            // invalid card id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return $this->stripeErrorRedirect('mycards');
            
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return $this->stripeErrorRedirect('mycards');
            
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return $this->stripeErrorRedirect('mycards');
            
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return $this->stripeErrorRedirect('mycards');
            
            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }
        
        return redirect()->route('mycards');
    }

    public function makeDefault($card)
    {
        $card_id = $card;
        /*
         * Get Customer Object from Stripe server
         * Customer Object for the current user
         */
        $customer = $this->getStripeCustomerObj();
        if ( !isset($customer->object) )
            return $this->stripeErrorRedirect('packages');

        try {
            $customer->default_source = $card_id;
            $customer->save();
        }
        catch(\Stripe\Error\Card $e) {
            return $this->stripeErrorRedirect('mycards');

            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Card-error-msg-';
        } 
        catch (\Stripe\Error\RateLimit $e) {
            return $this->stripeErrorRedirect('mycards');

            // Too many requests made to the API too quickly
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Ratelimit-error-msg-';
        } 
        catch (\Stripe\Error\InvalidRequest $e) {
            return $this->stripeErrorRedirect('mycards');

            // invalid card id provided
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-invalidRequest-error-msg-';
        } 
        catch (\Stripe\Error\Authentication $e) {
            return $this->stripeErrorRedirect('mycards');

            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Authentication-error-msg-';
        } 
        catch (\Stripe\Error\ApiConnection $e) {
            return $this->stripeErrorRedirect('mycards');

            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-ApiConnection-error-msg-';
        } 
        catch (\Stripe\Error\Base $e) {
            return $this->stripeErrorRedirect('mycards');

            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Base-error-msg-';
        } 
        catch (Exception $e) {
            return $this->stripeErrorRedirect('mycards');

            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err  = $body['error'];
          
            print('Status is:' . $e->getHttpStatus() . "\n");
            print('Type is:' . $err['type'] . "\n");
            print('Code is:' . $err['code'] . "\n");
            // param is '' in this case
            print('Param is:' . $err['param'] . "\n");
            print('Message is:' . $err['message'] . "\n");

            return '-Exception-error-msg-';
        }
        
        return redirect()->route('mycards');
    }

    protected function getStripeCustomerObj() 
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $stripeCustomer = StripeCustomer::CurrentUser()->first();
        if ($stripeCustomer) 
        {
            try {
                return \Stripe\Customer::retrieve($stripeCustomer->stripe_customer_id);
            }
            catch(\Stripe\Error\Card $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Card-error-msg-';
            } 
            catch (\Stripe\Error\RateLimit $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Too many requests made to the API too quickly
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Ratelimit-error-msg-';
            } 
            catch (\Stripe\Error\InvalidRequest $e) {
                return $this->stripeErrorRedirect('packages');
    
                // invalid customer id provided
                // Invalid parameters were supplied to Stripe's API
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-invalidRequest-error-msg-';
            } 
            catch (\Stripe\Error\Authentication $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Authentication-error-msg-';
            } 
            catch (\Stripe\Error\ApiConnection $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Network communication with Stripe failed
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-ApiConnection-error-msg-';
            } 
            catch (\Stripe\Error\Base $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Display a very generic error to the user, and maybe send
                // yourself an email
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Base-error-msg-';
            } 
            catch (Exception $e) {
                return $this->stripeErrorRedirect('packages');
    
                // Something else happened, completely unrelated to Stripe
                $body = $e->getJsonBody();
                $err  = $body['error'];
              
                print('Status is:' . $e->getHttpStatus() . "\n");
                print('Type is:' . $err['type'] . "\n");
                print('Code is:' . $err['code'] . "\n");
                // param is '' in this case
                print('Param is:' . $err['param'] . "\n");
                print('Message is:' . $err['message'] . "\n");
    
                return '-Exception-error-msg-';
            }
        }
        else 
        {
            $customer = \Stripe\Customer::create([
                'email' => Auth::user()->email,
                'description' => '',
                'metadata' =>  array(
                    "user_id" => Auth::user()->id,
                    "first_name" => Auth::user()->first_name,
                    "last_name" => Auth::user()->last_name,
                    "email" => Auth::user()->email,
                    "usertype_id" => Auth::user()->usertype_id,
                    "phone" => Auth::user()->phone,
                ),
            ]);

            $new_stripeCustomer = stripeCustomer::create([
                'user_id' => Auth::user()->id,
                'stripe_customer_id' => $customer->id,
            ]);

            return $customer;
        }
    }

    private function stripeErrorRedirect($route, $messages=null) 
    {
        if (is_array($messages) && !empty($messages)) 
        {
            //
        } 
        else 
        {
            $messages = array('Sorry, something went wrong. Please refresh the page and try again!');
        }

        return redirect()->route($route)->withErrors($messages);
    }

    // TEMP && TO BE DELETED
    public function createCustomerData(Request $request) 
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        
        $token = $request->stripeToken;

        $customer = \Stripe\Customer::create([
            'email' => 'qwerty4@gmail.com',
            'source' => $token,
        ]);
        dd($customer);
    }

    // UPDATE CUSTOMER DATA && TO BE DELETED
    public function updateCustomer() 
    {
        \Stripe\Stripe::setApiKey("sk_test_6BAsCN1aGsge5reBTIuCcYef");

        $cu = \Stripe\Customer::retrieve("cus_CUUrqlDwHNimro");
        $cu->description = "Customer for isabella.williams@example.com";
        $cu->source = "tok_visa"; // obtained with Stripe.js
        $cu->save();
    }

    // UPDATE CARD DATA && TO BE DELETED
    public function updateCard() 
    {
        \Stripe\Stripe::setApiKey("sk_test_6BAsCN1aGsge5reBTIuCcYef");

        $customer = \Stripe\Customer::retrieve("cus_CUUrg3fbDjMKDM");
        $card = $customer->sources->retrieve("card_1C5VrcEWwc5BdwQVDtaYGIaw");
        $card->name = "Anthony Martinez";
        $card->save();
    }

    // TEMP && TO BE DELETED
    public function test() 
    {
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        // Create a Customer:
        $customer = \Stripe\Customer::create([
            'source' => 'tok_mastercard',
            'email' => 'paying.user@example.com',
        ]);

        dd($customer);
        return;

        // Charge the Customer instead of the card:
        $charge = \Stripe\Charge::create([
            'amount' => 1000,
            'currency' => 'usd',
            'customer' => $customer->id,
        ]);

        // YOUR CODE: Save the customer ID and other info in a database for later.

        // When it's time to charge the customer again, retrieve the customer ID.
        $charge = \Stripe\Charge::create([
            'amount' => 1500, // $15.00 this time
            'currency' => 'usd',
            'customer' => $customer_id, // Previously stored, then retrieved
        ]);
    }
}
