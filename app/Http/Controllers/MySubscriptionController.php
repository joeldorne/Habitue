<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Package;
use App\MySubscription;

class MySubscriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function mysubscriptions() 
    {
        $mysubscriptions = MySubscription::isActive()->isVisible()->get();

        return view('mysubscriptions.mysubscriptions', compact('mysubscriptions'));
    }

    public function generateUniqueMySubscriptionSlug() 
    {
        do {
            $slug = 'ms_'.str_random(10);
        } while(MySubscription::Slug($slug)->exists());

        return $slug;
    }
}
