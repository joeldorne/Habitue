<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

use Auth;


class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        // temporarily commented for disabling subdomain
        // \Route::pattern('domain', '[a-z0-9.\-]+');

        parent::boot();

        // $this->bindRouteAgencyMember();

        $this->bindRouteProduct();

        $this->bindRouteProductService();

        // $this->bindRouteAgencyProduct();

        $this->bindRouteSubscription();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdminRoutes();

        $this->mapBuyerRoutes();

        $this->mapAgencyRoutes();

        $this->mapClientRoutes();

        $this->mapProductRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "admin" routes for the the application
     * 
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::prefix('admin')
             ->middleware('web')
             ->namespace($this->namespace.'\Admin')
             ->group(base_path('routes/admin.php'));
    }

    /**
     * Define the "buyer" routes for the the application
     * 
     * @return void
     */
    protected function mapBuyerRoutes()
    {
        Route::prefix('b')
             ->middleware('web')
             ->namespace($this->namespace.'\Buyer')
             ->group(base_path('routes/buyer.php'));
    }

    /**
     * Define the "agency" routes for the the application
     * 
     * @return void
     */
    protected function mapAgencyRoutes()
    {
        Route::prefix('a')
             ->middleware('web')
             ->namespace($this->namespace.'\Agency')
             ->group(base_path('routes/agency.php'));
    }

    /**
     * Define the "client" routes for the the application
     * 
     * @return void
     */
    protected function mapClientRoutes()
    {
        Route::prefix('c')
             ->middleware('web')
             ->namespace($this->namespace.'\Client')
             ->group(base_path('routes/client.php'));
    }

    /**
     * Define the "product" routes for the the application
     * 
     * @return void
     */
    protected function mapProductRoutes()
    {
        Route::prefix('p')
             ->middleware('web')
             ->namespace($this->namespace.'\Product')
             ->group(base_path('routes/product.php'));
    }

    protected function bindRouteAgencyMember()
    {
        Route::bind('agency_member', function ($slug) {
            return \App\User::where('slug', $slug)->firstOrFail();
        });

        // Route::model('agency_member', function ($agency_member) {//exit($slug);
        //     return App\User::where('slug', $agency_member)->firstOrFail();
        // }, function() {
        //     throw new NotFoundHTTPException;
        // });
    }

    protected function bindRouteAgencyProduct()
    {
        Route::bind('agency_product', function ($slug) {
            return \App\Product::where('slug', $slug)->firstOrFail();
        });
    }

    protected function bindRouteProduct()
    {
        Route::bind('product', function ($slug) {
            return \App\Product::where('slug', $slug)->with('productable')->firstOrFail();
        });
    }

    protected function bindRouteProductService()
    {
        Route::bind('product_service', function ($slug) {
            return \App\ProductService::where('slug', $slug)->with('productserviceable')->firstOrFail();
        });
    }

    protected function bindRouteSubscription()
    {
        Route::bind('subscription', function ($id) {
            return \App\Subscription::findOrFail($id);
        });
    }
}
