<?php

namespace App\Repositories\User;

use App\User;
use App\Repositories\BaseRepository;
use App\Repositories\User\Contracts\UserRepositoryInterface;
use App\Repositories\User\Exceptions\CreateUserErrorException;
use App\Repositories\User\Exceptions\UserNotFoundException;
use App\Repositories\User\Exceptions\UpdateUserErrorException;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use ErrorException;
use Exception;


class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    protected $model;

    public $allowed_roles = ['agency', 'client'];

    private static $slug_prefix = 'us_';

    /**
     * UserRepository constructor
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }


    /**
     * @return Collection of User
     */
    public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        $this->model->all();
    }


    /**
     * @param array $data
     * @return User
     * @throws CreateUserErrorException
     */
    public function create(array $data) : User
    {
        $data['slug'] = $this->generateSlug();

        try {
            $this->model = $this->model->create($data);
            return $this->model;
        } catch (ErrorException $e) {
            throw new CreateUserErrorException($e);
        }
    }


    /**
     * @param int $id
     * @return User
     * @throws UserNotFoundException
     */
    public function find(int $id) : User
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new UserNotFoundException($e);
        }
    }


    /** 
     * @param array $data
     * @return bool
     * @throws UpdateUserErrorException
     */
    public function update(array $data, $id) : bool
    {
        try {
            return $this->model->findOrFail($id)->update($data);
        } catch(Exception $e) {
            throw new UpdateUserErrorException($e);
        } catch(QueryException $e) {
            throw new UpdateUserErrorException($e);
        }
    }


    /** 
     * @param array $data
     * @param integer $id
     * @return bool
     * @throws UpdateUserErrorException
     */
    public function updateById(array $data, $id) : bool
    {
        $user = $this->find($id);

        try {
            return $user->update($data);
        } catch(\Exception $e) {
            throw new UpdateUserErrorException($e);
        }
    }


    /**
     * @return bool
     */
    public function delete($id) : bool
    {
        return $this->model->find($id)->delete();
    }


    public function show($id) : User
    {
        return $this->model->findOrFail($id);
    }


    public function getModel()
    {
        return $this->model;
    }


    public function setModel(User $model) : UserRepository
    {
        $this->model = $model;

        return $this;
    }


    public function with($relations)
    {
        return $this->model->with($relations);
    }


    /**
     * Generate a random string that will serve as the slug for the agency
     * Using a prefix to identify as slug for Agency
     * 
     * @return String slug
     */
    public static function generateSlug() : String
    {
        do {
            // $slug = self::$slug_prefix.mt_rand(100000000000000000, 999999999999999999).mt_rand(10, 99);
            $slug = self::$slug_prefix.str_random(20);
        } while(User::WhereSlug($slug)->exists());

        return $slug;
    }
}