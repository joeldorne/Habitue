<?php 

namespace App\Repositories\Product\Contracts;

use App\Repositories\BaseRepositoryInterface;

interface ProductRepositoryInterface extends BaseRepositoryInterface
{
    // public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'desc');

    // public function create(array $data);

    // public function update(array $data, $id);

    // public function delete($id);

    // public function show($id);
}