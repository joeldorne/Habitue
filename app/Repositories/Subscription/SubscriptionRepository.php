<?php

namespace App\Repositories\Subscription;

use App\ProductService as Subscription;
use App\Repositories\BaseRepository;
use App\Repositories\Subscription\Contracts\SubscriptionRepositoryInterface;
use App\Repositories\Subscription\Exceptions\CreateSubscriptionErrorException;
use App\Repositories\Subscription\Exceptions\SubscriptionNotFoundException;
use App\Repositories\Subscription\Exceptions\UpdateSubscriptionErrorException;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use ErrorException;
use Exception;


class SubscriptionRepository extends BaseRepository implements SubscriptionRepositoryInterface
{
    protected $model;

    private static $slug_prefix = 'su_';

    /**
     * SubscriptionRepository constructor
     * @param Subscription $subscription
     */
    public function __construct(Subscription $subscription)
    {
        $this->model = $subscription;
    }


    /**
     * @return Collection of Subscription
     */
    public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'asc') : Collection
    {
        $this->model->all();
    }


    /**
     * @param array $data
     * @return Subscription
     * @throws CreateSubscriptionErrorException
     */
    public function create(array $data) : Subscription
    {
        $data['slug'] = $this->generateSlug();

        try {
            return $this->model = $this->model->create($data);
        } catch (ErrorException $e) {
            throw new CreateSubscriptionErrorException($e);
        }
    }
    

    public function new(array $data) : Subscription
    {
        $data['slug'] = $this->generateSlug();

        $subscription = new Subscription($data);

        $this->setModel($subscription);

        return $subscription;
    }


    /**
     * @param int $id
     * @return Subscription
     * @throws SubscriptionNotFoundException
     */
    public function find(int $id) : Subscription
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new SubscriptionNotFoundException($e);
        }
    }


    /**
     * @param String $slug
     * @return Subscription
     * @throws SubscriptionNotFoundException
     */
    public function findWhereSlug(String $slug) : Subscription
    {
        try {
            return $this->model->Slug($slug)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new SubscriptionNotFoundException($e);
        }
    }


    /** 
     * @param array $data
     * @param Integer $id
     * @return bool
     * @throws UpdateSubscriptionErrorException
     */
    public function update(array $data, $id) : bool
    {
        try {
            return $this->model->findOrFail($id)->update($data);
        } catch(Exception $e) {
            throw new UpdateSubscriptionErrorException($e);
        } catch(QueryException $e) {
            throw new UpdateSubscriptionErrorException($e);
        }
    }


    /** 
     * @param array $data
     * @param String $slug
     * @return bool
     * @throws UpdateSubscriptionErrorException
     */
    public function updateWhereSlug(array $data, $slug) : bool
    {
        try {
            return $this->model->Slug($slug)->update($data);
        } catch(Exception $e) {
            throw new UpdateSubscriptionErrorException($e);
        } catch(QueryException $e) {
            throw new UpdateSubscriptionErrorException($e);
        }
    }


    /** 
     * @param array $data
     * @param integer $id
     * @return bool
     * @throws UpdateSubscriptionErrorException
     */
    public function updateById(array $data, $id) : bool
    {
        $subscription = $this->find($id);

        try {
            return $subscription->update($data);
        } catch(\Exception $e) {
            throw new UpdateSubscriptionErrorException($e);
        }
    }


    /**
     * @return bool
     */
    public function delete($id) : bool
    {
        return $this->model->find($id)->delete();
    }


    public function show($id) : Subscription
    {
        return $this->model->findOrFail($id);
    }


    public function getModel()
    {
        return $this->model;
    }


    public function setModel(Subscription $model) : SubscriptionRepository
    {
        $this->model = $model;

        return $this;
    }


    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function scopeSlug($slug)
    {
        $this->model->Slug($slug)->firstOrFail();
    }
    

    /**
     * Generate a random string that will serve as the slug for the subscription
     * Using a prefix to identify as slug for Subscription
     * 
     * @return String slug
     */
    public static function generateSlug() : String
    {
        do {
            // $slug = self::$slug_prefix.mt_rand(100000000000000000, 999999999999999999).mt_rand(100000000000000000, 999999999999999999).mt_rand(1000,9999);
            $slug = self::$slug_prefix.str_random(40);
        } while(Subscription::Slug($slug)->exists());

        return $slug;
    }




















    



























    /**  
    * Make a new instance of the entity to query on  
    *  
    * @param array $with  
    */  
    public function make(array $with = array())  
    {  
        return $this->model->with($with);  
    }

    /**  
    * Find an entity by id  
    *  
    * @param int $id  
    * @param array $with  
    * @return Illuminate\Database\Eloquent\Model  
    */  
    public function getById($id, array $with = array())  
    {  
        $query = $this->make($with);

        return $query->find($id);  
    } 

    /**  
    * Get Results by Page  
    *  
    * @param int $page  
    * @param int $limit  
    * @param array $with  
    * @return StdClass Object with $items and $totalItems for pagination  
    */  
    public function getByPage($page = 1, $limit = 10, $with = array())  
    {  
        // $result = new StdClass;  
        // $result->page = $page;  
        // $result->limit = $limit;  
        // $result->totalItems = 0;  
        // $result->items = array();

        // $query = $this->make($with);

        // $model = $query->skip($limit * ($page – 1))->take($limit)->get();

        // $result->totalItems = $this->model->count();  
        // $result->items = $model->all();

        // return $result;  
    }


    /**  
    * Return all results that have a required relationship
    * @param string $relation
    */  
    public function has($relation, array $with = array())  
    {  
        $entity = $this->make($with);

        return $entity->has($relation)->get();  
    }


    /**  
    * Find a single entity by key value  
    *  
    * @param string $key  
    * @param string $value  
    * @param array $with  
    */  
    public function getFirstBy($key, $value, array $with = array())  
    {  
        // $this->make($with)->where($key, ‘=’, $value)->first();  
    }


    /**  
    * Find many entities by key value  
    *  
    * @param string $key  
    * @param string $value  
    * @param array $with  
    */  
    public function getManyBy($key, $value, array $with = array())  
    {  
        // $this->make($with)->where($key, ‘=’, $value)->get();  
    }

    
}