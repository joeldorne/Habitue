<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientInvite extends Model
{



    protected $table = "clientinvites";
    protected $dates = [
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $fillable = [
        'agency_id',
        'invited_by',
        'email',
        'contact_name',
        'institution_name',
        'message',
        'token',
    ];

    

    public function agency() 
    {
        return $this->belongsTo('App\Agency');
    }



    public function scopeFromAgency($query, $agency_id)
    {
        return $query->where('agency_id', '=', $agency_id);
    }

    public function scopeWhereEmail($query, $email)
    {
        return $query->where('email', '=', $email);
    }

    public function scopeWhereToken($query, $token)
    {
        return $query->where('token', '=', $token);
    }


    
    public static function create(array $data)
    {
        $data['token'] = ClientInvite::generateToken();

        $model = static::query()->create($data);

        return $model;
    }

    public static function generateToken() 
    {
        do {
            $token = str_random(60);
        } while(ClientInvite::WhereToken($token)->exists());

        return $token;
    }
}
