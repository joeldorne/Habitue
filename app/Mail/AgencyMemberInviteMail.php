<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Agency;
use App\AgencyMemberInvite;

class AgencyMemberInviteMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "";
    protected $email;
    protected $token;
    protected $agency_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(AgencyMemberInvite $invite)
    {
        $this->email = $invite->email;
        $this->token = $invite->token;

        $agency = Agency::find($invite->agency_id);
        
        $this->agency_name = $agency->name;

        $this->subject = $this->agency_name." invites you to Habitue";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('mailables.agency-member-invite')
            ->subject($this->subject)
            ->with([
                'email' => $this->email,
                'token' => $this->token,
                'agency_name' => $this->agency_name,
            ]);
    }
}
