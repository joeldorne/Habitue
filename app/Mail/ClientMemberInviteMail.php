<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Client;
use App\ClientMemberInvite;

class ClientMemberInviteMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "";
    protected $email;
    protected $token;
    protected $client_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ClientMemberInvite $invite)
    {
        $this->email = $invite->email;
        $this->token = $invite->token;

        $client = Client::find($invite->client_id);
        
        $this->client_name = $client->name;

        $this->subject = $this->client_name." invites you to Habitue";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('mailables.client-member-invite')
            ->subject($this->subject)
            ->with([
                'email' => $this->email,
                'token' => $this->token,
                'client_name' => $this->client_name,
            ]);
    }
}
