<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Agency;
use App\ClientInvite;

class AgencyInviteClientMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "";
    protected $email;
    protected $token;
    protected $agency_name;
    protected $contact_name;
    protected $institution_name;
    protected $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ClientInvite $invite)
    {
        $this->email = $invite->email;
        $this->token = $invite->token;
        $this->contact_name = $invite->contact_name;
        $this->institution_name = $invite->institution_name;
        $this->message = $invite->message;

        $agency = Agency::find($invite->agency_id);
        
        $this->agency_name = $agency->name;
        $this->subject = $this->agency_name." invites you to Habitue.";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('mailables.agency-client-invite')
            ->subject($this->subject)
            ->with([
                'email' => $this->email,
                'token' => $this->token,
                'agency_name' => $this->agency_name,
                'contact_name' => $this->contact_name,
                'institution_name' => $this->institution_name,
                'message' => $this->message,
            ]);
    }
}
