<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'slug' => 'pd_'.str_random(40),

        'productable_type' => 'App\User',
        'productable_id' => 1,

        'name' => $faker->text(rand(5, 20)),
        'description' => $faker->sentence(rand(10, 20), true),

        'payment_method' => $faker->randomElement(['one time pay','subscription']),
        'pricing_type' => $faker->randomElement(['singular','variation']),

        'singular_price' => $faker->randomFloat(2, 1, 499.99),
        'singular_subscription_period' => $faker->randomElement([1,2,3,4,5,6,12]),

        'variation_prices' => json_encode([
            (object) [
                "name"=> $faker->words(rand(1,2), true), 
                "amount"=> $faker->randomFloat(2, 1, 499.99), 
                "period"=> $faker->randomElement([1,2,3,4,5,6,12]), 
                "default"=> false
            ],
            (object) [
                "name"=> $faker->words(rand(1,2), true), 
                "amount"=> $faker->randomFloat(2, 1, 499.99), 
                "period"=> $faker->randomElement([1,2,3,4,5,6,12]), 
                "default"=> false
            ],
            (object) [
                "name"=> $faker->words(rand(1,2), true), 
                "amount"=> $faker->randomFloat(2, 1, 499.99), 
                "period"=> $faker->randomElement([1,2,3,4,5,6,12]), 
                "default"=> true
            ],
        ]),

        'activate_form_url' => $faker->url(),

        'disabled' => $faker->randomElement([0,1]),
    ];
});




// {"name":"'.$faker->words(rand(1,2), true).'","amount":"'.$faker->randomFloat(2, 1, 499.99).'","month":"'.$faker->randomElement([1,2,3,4,5,6,12]).'","default":"false"},
// {"name":"'.$faker->words(rand(1,5), true).'","amount":"'.$faker->randomFloat(2, 1, 499.99).'","month":"'.$faker->randomElement([1,2,3,4,5,6,12]).'","default":"true"},
// {"name":"'.$faker->words(rand(1,5), true).'","amount":"'.$faker->randomFloat(2, 1, 499.99).'","month":"'.$faker->randomElement([1,2,3,4,5,6,12]).'","default":"false"}