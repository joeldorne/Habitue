<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            [
                'created_by' => 3,
                'slug' => 'cl_'.str_random(10),
                'name' => 'Client of UserTwo',
                'website_url' => 'clientof@usertwo.com',
                'country' => 'UK',
                'phone' => '987654321',
                'subdomain' => 'cut',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);

        DB::table('client_user')->insert([
            [
                'user_id' => 3,
                'client_id' => 1,
                'position' => 'Administrator',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
