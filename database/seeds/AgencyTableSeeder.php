<?php

use Illuminate\Database\Seeder;

class AgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agencies')->insert([
            [
                'created_by' => 2,
                'slug' => 'ag_'.str_random(10),
                'name' => 'Agency of UserOne',
                'website_url' => 'agencyof@userone.com',
                'country' => 'US',
                'phone' => '123456789',
                'subdomain' => 'auo',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);

        DB::table('agency_user')->insert([
            [
                'user_id' => 2,
                'agency_id' => 1,
                'position' => 'Administrator',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
