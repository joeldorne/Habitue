<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'slug' => 'us_'.str_random(20),
                'name' => 'Admin',
                'first_name' => 'Ad',
                'last_name' => 'Min',
                'email' => 'admin@habitue.com',
                'password' => bcrypt('admin'),
                'role_id' => 1,
                'created_at' => \Carbon\Carbon::now()->toDateString(),
                'updated_at' => \Carbon\Carbon::now()->toDateString(),
            ],
            [
                'slug' => 'us_'.str_random(20),
                'name' => 'User One',
                'first_name' => 'User',
                'last_name' => 'One',
                'email' => 'user.one@habitue.com',
                'password' => bcrypt('qwqwqw'),
                'role_id' => 4,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'slug' => 'us_'.str_random(20),
                'name' => 'User Two',
                'first_name' => 'User',
                'last_name' => 'Two',
                'email' => 'user.two@habitue.com',
                'password' => bcrypt('qwqwqw'),
                'role_id' => 4,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            // [
            //     'slug' => 'us_'.str_random(20),
            //     'name' => 'Agency One',
            //     'first_name' => 'Agency',
            //     'last_name' => 'One',
            //     'email' => 'agency.one@habitue.com',
            //     'password' => bcrypt('qwqwqw'),
            //     'role_id' => 2,
            //     'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            //     'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            // ],
            // [
            //     'slug' => 'us_'.str_random(20),
            //     'name' => 'Client One',
            //     'first_name' => 'Client',
            //     'last_name' => 'One',
            //     'email' => 'client.one@habitue.com',
            //     'password' => bcrypt('qwqwqw'),
            //     'role_id' => 3,
            //     'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            //     'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            // ],
            // [
            //     'slug' => 'us_'.str_random(10),
            //     'name' => 'One Buyer',
            //     'first_name' => 'One',
            //     'last_name' => 'Buyer',
            //     'email' => 'one.buyer@habitue.com',
            //     'password' => bcrypt('qwqwqw'),
            //     'role_id' => 4,
            //     'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            //     'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            // ],
        ]);

        DB::table('profiles')->insert([
            [
                'user_id' => 1,
                'position' => 'Administrator',
            ],
            [
                'user_id' => 2,
                'position' => 'User',
            ],
            [
                'user_id' => 3,
                'position' => 'User',
            ],
        ]);
    }
}
