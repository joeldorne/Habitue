<?php

use Illuminate\Database\Seeder;

class StripesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stripes')->insert([
            [
                'stripecustomerable_type' => 'App\User',
                'stripecustomerable_id' => 2,
                'stripe_id' => 'cus_CVeJlhBX3p0JqV',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'stripecustomerable_type' => 'App\User',
                'stripecustomerable_id' => 3,
                'stripe_id' => 'cus_CVaZxOwo2Lria4',
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ]);
    }
}
