<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgencyTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('created_by')->nullable();
            $table->string('slug', 23)->unique();
            $table->string('name', 50);
            $table->string('website_url')->nullable();
            $table->string('country', 5)->nullable();
            $table->string('phone')->nullable();
            $table->string('subdomain', 15)->nullable();
            $table->tinyInteger('hide')->default(0);
            $table->tinyInteger('disabled')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('agency_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('agency_id');
            $table->string('position')->default('member');
            $table->timestamps();

            $table->unique(['user_id', 'agency_id']);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('agency_id')->references('id')->on('agencies')->onDelete('cascade');
        });

        Schema::create('agencymemberinvites', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('agency_id');
            $table->unsignedInteger('invited_by');
            $table->string('email', 200);
            $table->string('token', 60);
            $table->softDeletes();
            $table->timestamps();
        });

        // Schema::create('clientinvites', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->unsignedInteger('agency_id');
        //     $table->unsignedInteger('invited_by');
        //     $table->string('email', 200);
        //     $table->string('contact_name', 50)->nullable();
        //     $table->string('institution_name', 100)->nullable();
        //     $table->text('message')->nullable();
        //     $table->string('token', 60);
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
        Schema::dropIfExists('agency_user');
        Schema::dropIfExists('agencymemberinvites');
        // Schema::dropIfExists('clientinvites');
    }
}
