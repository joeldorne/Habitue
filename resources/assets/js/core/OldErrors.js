class Errors {

  constructor() {

    this.errors = {};

  }


  has(field) {
    
    return this.errors.hasOwnProperty(field);

  }


  any() {

    return Object.keys(this.errors).length > 0;

  }


  getfield(field) {

    if (this.errors[field]) {

      return this.errors[field][0];

    }

  }

  getMessages() {

    let msgs = [];


    _.forOwn(this.errors, function(value, key) { 

      _.forEach(value, function(v, k) {

        msgs.push(v);

      });

    });

    msgs = _.uniq(msgs);

    return msgs;

  }


  record(errors) {

    this.errors = errors;

  }


  clear(field) {

    delete this.errors[field];

  }

  clearAll() {

    this.errors = {};

  }

}

export default Errors;