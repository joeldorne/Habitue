$('#side-menu-comment')
  .popup({
    position : 'right center',
    title    : 'Chats',
    content  : ''
  })
;

$('#side-menu-filestorage')
  .popup({
    position : 'right center',
    title    : 'File Storage',
    content  : ''
  })
;

$('#side-menu-tasks')
  .popup({
    position : 'right center',
    title    : 'Tasks',
    content  : ''
  })
;

$('#side-menu-users')
  .popup({
    position : 'right center',
    title    : 'Users',
    content  : ''
  })
;

$('#side-menu-members')
  .popup({
    position : 'right center',
    title    : 'Members',
    content  : ''
  })
;

$('#side-menu-clients')
  .popup({
    position : 'right center',
    title    : 'Clients',
    content  : ''
  })
;

$('#side-menu-products')
  .popup({
    position : 'right center',
    title    : 'Products',
    content  : ''
  })
;

$('#side-menu-subscriptions')
  .popup({
    position : 'right center',
    title    : 'Subscriptions',
    content  : ''
  })
;

$('#side-menu-purchases')
  .popup({
    position : 'right center',
    title    : 'Purchases',
    content  : ''
  })
;

$('#side-menu-paymentreports')
  .popup({
    position : 'right center',
    title    : 'Payment Reports',
    content  : ''
  })
;

$('#side-menu-stores')
  .popup({
    position : 'right center',
    title    : 'Shop',
    content  : ''
  })
;

$('#side-menu-settings')
  .popup({
    position : 'right center',
    title    : 'Settings',
    content  : ''
  })
;