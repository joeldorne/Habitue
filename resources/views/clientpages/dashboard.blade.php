@extends('layouts.auth_all')


@section('title')
Dashboard
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main fluid container segment basic">
        <h3>Dashboard</h3>

        <p><a href="{{ route('client.profile') }}">{{$myGroup->name}} Profile</a></p>
    </div>
@endsection


@section('script-footer')
@endsection