@extends('layouts.auth_all')


@section('title')
Group Profile
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main container segment basic">
        <p>Group Name: {{ Auth::user()->myClient()->name }}</p>
        <p>Website: {{ Auth::user()->myClient()->website_url }}</p>
        <p>Country: {{ Auth::user()->myClient()->country }}</p>
        <p>Phone: {{ Auth::user()->myClient()->phone }}</p>
        <p>Subdomain: {{ Auth::user()->myClient()->subdomain }}</p>

        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <p><a href="{{ route('client.profile.edit') }}">Update Profile</a></p>
    </div>
@endsection


@section('script-footer')
@endsection