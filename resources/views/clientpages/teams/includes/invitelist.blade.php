<div class="ui segment basic grid">
  <h4 class="ui small left floated header">
    Invites
  </h4>
</div>

<div class="ui segment basic grid">
  <div class="ui small form {{ ($errors->any()) ? 'error' : '' }} {{ (session('success')) ? 'success' : '' }}">
        
    @if (session('success'))
      <div class="ui success message">
        <div class="header">Success</div>
        <p>{{ session('success') }}</p>
      </div>
    @endif

    @if($errors->any())
      <div class="ui error message">
        <div class="header">There were some errors with your invitation</div>
        <ul class="list">
          @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif

    <form id="form_team_invite" method="post" action="{{ route('client.team.invite.store') }}">
      {{ csrf_field() }}

      <div class="fields">
        <div class="field">
          <input type="email" name="email" placeholder="email" value="{{ old('email') }}" required>
        </div>

        <div class="field">
          <input type="submit" id="btn_submit" class="ui small submit button" value="Invite">
        </div>
      </div>
    </form>
  </div>
</div>

<div class="ui segment basic grid">
  @if (Auth::user()->client()->first()->memberInvites->count() > 0)
    <div class="ui bulleted list">
      @foreach(Auth::user()->client()->first()->memberInvites->reverse() as $invite)
        <div class="item">
          <div class="content">
            <div class="header">
              {{ $invite->email }} 
            </div>
            <div class="small description">{{ $invite->updated_at->format('M d, Y - h:i:s A') }}</div>
            <div>
              <a href="" data-tooltip="resend email to {{ $invite->email }}" data-position="top center">
                <i class="mail icon"></i>
              </a>
              <a href="" data-tooltip="recede invitation to {{ $invite->email }}" data-position="top center">
                <i class="times red icon"></i>
              </a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  @else
    <div class="ui bulleted list">
      <div class="item">
        <div class="content">
          <div class="small description">
            No invitation found. 
          </div>
        </div>
      </div>
    </div>
  @endif
</div>