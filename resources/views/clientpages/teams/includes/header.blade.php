<div class="ui main fluid segment basic">
  <div class="ui breadcrumb">
    <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
    <span class="divider">/</span>
    <div class="active section">
      <i class="users icon"></i> 
      Teams 
      ({{ Auth::user()->client[0]->members->count() }})
    </div>
  </div>
</div>