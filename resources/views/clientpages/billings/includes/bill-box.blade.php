<div class="card">
  <div class="content">
    <div class="ui form">
      <div class="field">
        <div class="ui fluid transparent input">
          <select class="ui compactx selection dropdown">
            <option selected="" value="">Choose Client</option>
            <option value="articles">Client 1</option>
            <option value="products">Client 2</option>
          </select>
        </div>
      </div>
    </div>
  </div>

  <div class="content">
    <div class="ui form">
      <div class="field">
        <div class="ui fluid transparent left icon input">
          <i class="dollar sign icon"></i>
          <input type="text" name="cost" value="" placeholder="Enter cost">
        </div>
      </div>
    </div>
  </div>

  <div class="content">
    <div class="header">
      Client Name
    </div>
    <div class="meta">
      more details
    </div>
    <div class="description">
      Payments for ... details ...
    </div>
  </div>
  
  <div class="extra content">
    <div class="ui three buttons">
      <div class="ui basicx red button">Delete</div>
      <div class="ui basicx blue button">Save</div>
      <div class="ui basicx green button">Send</div>
    </div>
  </div>
</div>