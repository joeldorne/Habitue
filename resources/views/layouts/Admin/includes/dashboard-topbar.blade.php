<div class="ui stackable large menu attached">
  <div class="header item">
    <a href="/">{{ config('app.name') }}</a>
  </div>

  <div class="right menu">
    <a id="top-menu-notify" class="item">
      <i class="bell outline icon"></i> 
      <span class="ui circular mini label">0</span>
    </a>
    <div class="ui simple dropdown item">
      <i class="user icon"></i>
      {{ Auth::user()->fullname }}
      <i class="dropdown icon"></i>
      <div class="menu">
        @if (Auth::user()->hasRole('admin'))
          <a class="item" href="/admin"><i class="user secret icon"></i> Admin</a>
        @endif
        <a class="item" href=""><i class="address book icon"></i> Profile</a>
        <a class="item" href="{{ route('admin.settings.stripe.migration') }}"><i class="cog icon"></i> Manage Stripe Migration</a>
        <a class="item" href=""><i class="credit card outline icon"></i> Billings</a>
        <a class="item" 
          href="{{ route('logout') }}" 
          onclick="event.preventDefault();document.getElementById('logout-form').submit();">
          <i class="power off icon"></i> 
          Logout
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </div>
    </div>
  </div>
</div>