<div class="ui large menu attached">
  <div class="header item">
    <a href="/">{{ config('app.name') }}</a>
  </div>

  <div class="right menu">
    <a class="item">
      <i class="bell outline icon"></i>
    </a>
    <div class="ui simple dropdown item">
      <i class="user icon"></i>
      {{ Auth::user()->name }}
      <i class="dropdown icon"></i>
      <div class="menu">
        @if (Auth::user()->hasRole('admin'))
          <a class="item" href="/admin"><i class="user secret icon"></i> Admin</a>
        @endif
        <a class="item" href="{{ route('profile') }}"><i class="address book icon"></i> Profile</a>
        <a class="item"><i class="cog icon"></i> Account Settings</a>
        <a class="item" 
          href="{{ route('logout') }}" 
          onclick="event.preventDefault();document.getElementById('logout-form').submit();">
          <i class="power off icon"></i> 
          Logout
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </div>
    </div>
  </div>
</div>