<div class="ui visible inverted left vertical sidebar menu very thin icon">
  <a class="item" href="/a/dashboard">
    <i class="grid layout icon"></i>
  </a>
  <a class="item hide" id="side-menu-comment" href="/a/chats"><i class="comments icon"></i></a>
  <a class="item hide" id="side-menu-filestorage"><i class="briefcase icon"></i></a>
  <a class="item hide" id="side-menu-tasks" href="/a/chats"><i class="tasks icon"></i></a>
  <a class="item" id="side-menu-members" href="/a/team"><i class="users icon"></i></a>
  <a class="item" id="side-menu-clients" href="/a/clients" style="display:none"><i class="building icon"></i></a>
  <a class="item" id="side-menu-products" href="/a/products"><i class="tv icon"></i></a>
  <a class="item" id="side-menu-paymentreports" href="/a/paymentreports"><i class="dollar sign icon"></i></a>
</div>