<div class="ui large menu attached">
  <div class="container">
    <div class="header item">
      <a href="/">{{ config('app.name') }}</a>
    </div>

    <div class="right menu">
      <div class="ui simple dropdown item">
        <i class="user icon"></i>
        {{ Auth::user()->fullname }}
        <i class="dropdown icon"></i>
        <div class="menu">
          @if (Auth::user()->hasRole('admin'))
            <a class="item" href="/admin"><i class="user secret icon"></i> Admin</a>
          @endif
          <a class="item" href="{{ route('user.profile') }}"><i class="address book icon"></i> Profile</a>
          <a class="item" 
            href="{{ route('logout') }}" 
            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <i class="power off icon"></i> 
            Logout
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>
  </div>
</div>