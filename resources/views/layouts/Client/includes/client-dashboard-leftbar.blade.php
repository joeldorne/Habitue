<div class="ui visible inverted left vertical sidebar menu very thin icon">
  <a class="item" href="/c/dashboard">
    <i class="grid layout icon"></i>
  </a>
  <a class="item hide" id="side-menu-filestorage"><i class="briefcase icon"></i></a>
  <a class="item" id="side-menu-members" href="/c/team"><i class="users icon"></i></a>
  <a class="item" id="side-menu-purchases" href="/c/purchases"><i class="money bill alternate outline icon"></i></a>

  <a class="item" id="side-memu-stores" style="margin-top:100px" href="{{route('products.list')}}" target="_new"><i class="shoppng cart icon"></i></a>
</div>