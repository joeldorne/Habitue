@extends('layouts.auth_all')


@section('title')
Purchases
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main fluid container segment basic">
        <h3>My Purchases</h3>

        
    </div>

    <products-purchases-list :productspurchased="{{ $purchases }}"></products-purchases-list>
@endsection


@section('script-footer')
@endsection