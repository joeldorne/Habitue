@extends('layouts.auth_all')


@section('title')
My Subscription
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main fluid container segment basic">
        <h3>My Subscriptions</h3>

        @foreach ($mysubscriptions as $subscription)
            <div class="ui relaxed divided list">
                <div class="item">
                    <i class="large github middle aligned icon"></i>
                    <div class="content">
                        <a class="header">{{ $subscription->nickname }}</a>

                        <div class="description">
                            duration: {{ $subscription->obj_stripe_plan->interval_count }} {{ $subscription->obj_stripe_plan->interval }}
                        </div>

                        @if ($subscription->trial_ends_at != null)
                            <div class="description">Trial days left: {{ $subscription->trial_ends_at->format('M. d, Y | h:m') }}</div>
                        @endif

                        <div class="description">
                            Status: 
                            @if ($subscription->obj_stripe_subs->status == 'trialing')
                                on trial 
                            @else
                                {{ $subscription->obj_stripe_subs->status }}
                            @endif
                        </div>

                        <div class="description">
                            <a href="{{ route('buyer.mysubscription.cancel', $subscription->id) }}" class="ui button mini red">Cancel</a>
                            
                            <a href="{{ route('buyer.mysubscription.downloadInvoice', $subscription->id) }}" class="ui button mini blue" style="display:none">Download Invoice</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        
    </div>
@endsection


@section('script-footer')
@endsection