@extends('layouts.auth_all')


@section('title')
Subscriptions
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main fluid container segment basic">

        <h3>Shop Subscriptions</h3>

        @include('buyerpages.shop.partials.cardlist-subscription', $subscriptions)

    </div>
@endsection


@section('script-footer')
@endsection