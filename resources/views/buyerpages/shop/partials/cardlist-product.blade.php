<div class="ui link cards">

    @foreach($products as $product)
        <div class="ui card">


            <div class="image">  
                <img src="/{{ $product->image }}">
            </div>


            <div class="content">
                <a class="header">{{ $product->name }}</a>

                <div class="description">
                    {{ $product->description }}
                </div>
            </div>


            <div class="extra content">
                @if ( $product->pricing_type == 'singular' )
                    <span class="">
                        $ {{ $product->singular_price }}
                    </span>
                @else
                    <span class="">
                        @include('buyerpages.shop.partials.dropdown-variation-prices', 
                        [
                            'array_prices' => $product->variation_prices_as_array(), 
                            'default_price' => $product->variation_prices_default_for_dropdown()
                        ])
                    </span>
                @endif
            </div>


            <div class="extra content">
                @if ( $product->subscription == 1 )
                    <a class="ui right floated teal button">
                        <i class="money bill alternate outline icon"></i>
                        Subscribe
                    </a>
                @else
                    <a href="{{ route('buyer.shop.purchasing', $product->slug) }}" class="ui right floated primary button">
                        <i class="money bill alternate outline icon"></i>
                        Purchase
                    </a>
                @endif
            </div>


        </div>
    @endforeach
    
</div>