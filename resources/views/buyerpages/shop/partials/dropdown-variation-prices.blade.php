<div class="ui fluid selection dropdown">
    <input id="product_price_select" type="hidden" name="price" value="{{$default_price}}">
    <i class="dropdown icon"></i>
    <div class="default text">Price</div>
    <div class="menu">

        @foreach ($array_prices as $price)
            <div class="item" data-value="{{$price->name}} - {{$price->amount}}">{{$price->name}} - ${{$price->amount}}</div>
        @endforeach
    </div>
</div>
