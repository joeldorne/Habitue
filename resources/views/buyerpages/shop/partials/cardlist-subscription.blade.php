<div class="ui link cards">

    @foreach($subscriptions as $subscription)
        <div class="ui card">


            <div class="image">  
                <img src="/{{ $subscription->image }}">
            </div>


            <div class="content center aligned">
                <a class="header">{{ $subscription->name }}</a>
            </div>


            @if ($subscription->description)
                <div class="content">
                    <div class="description">
                        {{ $subscription->description }}
                    </div>
                </div>
            @endif


            <div class="center aligned extra content">
                <a class="ui blue button" href="{{ route('buyer.shop.subscription', $subscription->slug) }}">Subscribe</a>
            </div>


        </div>
    @endforeach
    
</div>