@extends('layouts.auth_all')


@section('title')
Subscribing
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main fluid container segment basic">

        <h3>Subscribing</h3>

        
        <div class="ui stackable grid">

            <!-- Subscription Info -->
            <div class="ui six wide column">
                <div class="ui cards">
                    <div class="card">
                        <div class="content">
                            <div class="header">{{ $subscription->name }}</div>
                            <div class="description">
                                {{ $subscription->description }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Subscription Info -->


            @if (isset($status) && $status == 'subscribed')
                
                <div class="ui ten wide column">
                    <div class="ui visible message">
                        <p>You are still currently subscribed with this product. Go to <a href="{{ route('buyer.mysubscription') }}">my subscription</a>.</p>
                    </div>
                </div>

            @else
                <!-- Billing Plans -->
                <div class="ui ten wide column">
                    <div class="ui relaxed divided list">
                        @foreach($subscription->stripe_ps->plans->data as $plan)
                            <div class="item">
                                <div class="content">
                                    <div class="header">{{ $plan->nickname }}</div>
                                    <div class="description">
                                        {{ strtoupper($plan->currency) }} 
                                        {{ number_format($plan->amount/100, 2) }} 
                                        per 
                                        {{ $plan->interval_count }} 
                                        {{ $plan->interval }}(s) 
                                        <a 
                                        href="{{ route('buyer.subscription.checkoutform', [
                                            'product_service' => $subscription->slug, 
                                            'stripe_plan' => $plan->id
                                        ]) }}" 
                                        class="ui small positive button">Checkout</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- End Billing Plans -->
            @endif

        </div>

    </div>
@endsection


@section('script-footer')
@endsection