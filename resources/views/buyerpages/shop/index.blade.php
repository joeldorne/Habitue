@extends('layouts.auth_all')


@section('title')
Shop
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main fluid container segment basic">

        <div class="ui two small statistics">
            <div class="statistic">
                <div class="value">
                    <a href="{{ route('buyer.shop.products') }}">
                        Products
                    </a>
                </div>
                <div class="label">
                    <a href="{{ route('buyer.shop.products') }}">
                        Browse 100
                    </a>
                </div>            
            </div>
            <div class="statistic">
                <div class="value">
                    <a href="{{ route('buyer.shop.subscriptions') }}">
                        Subscriptions
                    </a>
                </div>
                <div class="label">
                    <a href="{{ route('buyer.shop.subscriptions') }}">
                        Browse 12
                    </a>
                </div>
            </div>
        </div>

    </div>
@endsection


@section('script-footer')
@endsection