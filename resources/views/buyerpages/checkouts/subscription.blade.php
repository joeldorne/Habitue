@extends('layouts.auth_all')


@section('title')
Checkout Subscription
@endsection


@section('script-header')
    <script src="https://js.stripe.com/v3/"></script>

    <script src="{{URL::asset('vendor/stripe-elements-example/index.js')}}" data-rel-js></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">

    <link href="{{URL::asset('vendor/stripe-elements-example/base.css')}}" rel="stylesheet" type="text/css" data-rel-css="" />
    <link href="{{URL::asset('vendor/stripe-elements-example/example2/style.css')}}" rel="stylesheet" type="text/css" data-rel-css="" />
@endsection


@section('content')
    <div class="ui main fluid container segment basic">


        <h3>Checkout Subscription</h3>

        
        <div class="ui stackable grid">

            <!-- Stripe Checkout Form -->
            <div class="ui ten wide column">

                <div class="ui main container segment basic">
                    @include('buyerpages.checkouts.partials.stripe-checkout-form')
                </div>

            </div>
            <!-- End Stripe Checkout Form -->


            <!-- Payment Info -->
            <div class="ui six wide column">
                <div class="ui main container segment basic">

                    <div class="ui centered card">
                        <div class="content">
                            <div class="ui sub header">Subscribing for:</div>
                        </div>

                        <div class="content">
                            <span>Product: {{ $subscription->name }}</span>
                        </div>

                        <div class="content">
                            <div>Duration: {{ $plan->interval_count }} {{ $plan->interval }}(s)</div>
                        </div>

                        <div class="content">
                            <div class="header">Total: {{ strtoupper($plan->currency) }} {{ number_format($plan->amount/100, 2) }}</div>
                        </div>


                        <div class="content center aligned">
                            <div class="ui positive small button" id="btn_s_checkout">Checkout</div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End Payment Info -->

        </div>

    </div>
@endsection


@section('script-footer')
    <script>
        var stripe = Stripe('{{ config("services.stripe.key") }}');
    </script>


    <script src="{{URL::asset('vendor/stripe-elements-example/l10n.js')}}" data-rel-js></script>

    <script src="{{URL::asset('vendor/stripe-elements-example/example2/script.js')}}" data-rel-js></script>

    <script>
        var form = document.querySelector("#form-stripe");
        var form_btn_submit = document.querySelector("#form_btn_submit");
        
        var btn_s_checkout = document.querySelector("#btn_s_checkout");

        btn_s_checkout.addEventListener('click', function(){
            form_btn_submit.click();
        });

    </script>
@endsection