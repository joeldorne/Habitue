@extends('layouts.auth_all')


@section('title')
Payment Reports
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main fluid basic">
    <div class="ui horizontal segments">


      <div class="ui segment">
        <div class="ui one statistics">
          <div class="statistic">
            <div class="value">
              {{ Auth::user()->myAgency()->getSaleCount() }}
            </div>
            <div class="label">
              <a href="/a/paymentreports/purchases">Products Sold</a>
            </div>
          </div>
        </div>
      </div>


      <div class="ui segment">
        <div class="ui one statistics">
          <div class="statistic">
            <div class="value">
              $ {{ Auth::user()->myAgency()->getTotalSales() }}
            </div>
            <div class="label">
              <a>Total Sales</a>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>


  @include('agencypages.paymentreports.includes.breadcrumbs')


  <div class="ui main fluid container basic">



    @if ($products_sold->count() > 0)
      <div class="ui grid">
        <div class="eight wide column">

          <table class="ui sortable celled table">
            <thead>
              <tr class="positive">
                <td colspan="4">
                  Recent Products Sold 
                  <a href="/a/paymentreports/purchases">view all</a>
                </td>
              </tr>
            </thead>
            <thead>
              <tr>
                <th>Product Name</th>
                <th>Buyer</th>
                <th>Price</th>
                <th>Date of Purchase</th>
              </tr>
            </thead>
            <tbody>


              @foreach($products_sold as $purchase)

                <tr>
                  <td>{{ $purchase->product->name }}</td>
                  <td>{{ $purchase->buyer->name }}</td>
                  <td>${{ $purchase->price }}</td>
                  <td>{{ $purchase->created_at->format('M d, Y') }}</td>
                </tr>

              @endforeach


            </tbody>
          </table>

        </div>

        <div class="eight wide column">



        </div>

      </div>
    @endif



  </div>
@endsection


@section('script-footer')
@endsection