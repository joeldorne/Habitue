@extends('layouts.auth_all')


@section('title')
Products
@endsection


@section('script-header')
  <link href="{{URL::asset('vendor/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" />
@endsection


@section('content')
  @include('agencypages.products.includes.breadcrumbs')

  <div class="ui main fluidx container segment basic">

    <product-edit-form-with-cropper :originalproduct="{{$product}}"></product-edit-form-with-cropper>

  </div>


@endsection


@section('script-footer')
  <script src="{{URL::asset('vendor/cropper/cropper.min.js')}}"></script>
@endsection