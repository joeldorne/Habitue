<table class="ui padded stackable table">
  <thead class="full-width">
    <tr class="warning">
      <th colspan="5">
        <a href="/a/products/create" class="ui right floated small primary labeled icon button">
          <i class="plus icon"></i> Add Product
        </a>

        <a href="{{ Auth::user()->agency[0]->getPublicProductPagePath() }}" target="_blank" class="ui right floated small orange button">
           View Public Page 
        </a>
      </th>
    </tr>
  </thead>
  <thead>
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Price(s)</th>
      <th class="center aligned"><i class="cog icon"></i></th>
    </tr>
  </thead>
  <tbody>


    @foreach ($products->reverse() as $product)

      <tr>

        <td>
          <h4 class="ui image header">

            @if(file_exists( $product->image )) 
              <img src="/{{ $product->image }}" class="ui mini rounded image">
            @else
              <img src="/storage/products/default.png" class="ui mini rounded image">
            @endif

            <div class="content">
              {{ $product->name }}
              <div class="sub">
                @if ( $product->disabled == 1 )
                  <span class="ui red small label">inactive</span>
                @endif
                @if ( $product->subscription == 1 )
                  <span class="ui small label">subscription</span>
                @endif
              </div>
            </div>

          </h4>
        </td>

        <td>{{ $product->description }}</td>

        @if ($product->price_type == 'singular')
          <td>$ {{ $product->price }}</td>
        @else
          <td> {{ get_data_from_price_variation($product->price_variations) }} </td>
        @endif

        <td class="center aligned">
          <div class="ui dropdown item">
            <i class="grey ellipsis horizontal icon"></i>
            <div class="menu">
              <a href="/a/products/edit/{{ $product->slug }}" class="green item">Edit</a>
              @if ( $product->disabled == 1 )
                <a href="/a/products/settoactive/{{$product->slug}}" class="item">Set this product to active</a>
              @else
                <a href="/a/products/settoinactive/{{$product->slug}}" class="item">Set this product to inactive</a>
              @endif
              <a class="red item">Permanently Remove</a>
            </div>
          </div>
        </td>

      </tr>

    @endforeach


  </tfoot>
</table>