@extends('layouts.auth_all')


@section('title')
Products
@endsection


@section('script-header')
@endsection


@section('content')
  @include('agencypages.products.includes.breadcrumbs')

  <div class="ui main fluid container segment basic">

    
    @include('agencypages.products.includes.product-table-list', $products)


  </div>
@endsection


@section('script-footer')
@endsection