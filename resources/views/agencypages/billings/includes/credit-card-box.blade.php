<div class="five wide column">
  <div class="ui divided items">
    <div class="item">
      <div class="ui tiny image">
        <img src="/images/semantics/wireframe/image.png">
      </div>
      <div class="content">
        <span class="header">#### #### #### {{ $card->last4 }}</span>
        <div class="meta">
          <span class="cinema">{{ $card->brand }}</span>
        </div>
        <div class="meta">
          <p>From Client Name</p>
        </div>
        <div class="extra">
          @if ($default_card != $card->id)
            <a class="ui label" href="/a/billings/cards/{{$card->id}}/makedefault">Make Default</a>
            
            <a class="ui red label" href="/a/billings/cards/{{$card->id}}/remove"><i class="times icon"></i> Delete</a>
          @else
            <div class="ui label basic">Default Card</div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>