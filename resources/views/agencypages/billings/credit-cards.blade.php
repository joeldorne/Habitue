@extends('layouts.auth_all')


@section('title')
Cards - {{ config('app.name') }}
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main segment basic">
    
    <div class="ui stackable grid">

      <div class="three wide column">
        <div class="ui breadcrumb">
          <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
          <span class="divider">/</span>
          <div class="active section">Cards</div>
        </div>

        @include('agencypages.billings.includes.leftsubmenu')
      </div>

      <div class="thirteen wide column">
        @if ($customer->sources->total_count > 0)

          <div class="ui stackable grid">
            @foreach ($customer->sources->data as $card)
              @include('agencypages.billings.includes.credit-card-box', ['card'=>$card, 'default_card'=>$customer->default_source])
            @endforeach

            <div class="five wide column">
              <div class="ui divided items">
                <div class="item">
                  <div class="ui tiny image">
                    <img src="/images/semantics/wireframe/image.png">
                  </div>
                  <div class="content">
                    <span class="header"><a href="{{ route('agency.billings.cards.add') }}">Add Card</a></span>
                  </div>
                </div>
              </div>
            </div>

          </div>

        @else

          <div class="ui message">
            <div class="header">
              You haven't added a billing card yet
            </div>
            <p>To add a billing card please click <a href="{{ route('agency.billings.cards.add') }}">here</a>.</p>
          </div>

        @endif
      </div>
      
    </div>

  </div>
@endsection


@section('script-footer')
@endsection