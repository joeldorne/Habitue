@extends('layouts.agency-dashboard')


@section('title')
Dashboard - {{ config('app.name') }}
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">Clients</div>
    </div>

    <div class="ui stackable grid">
      <div class="ten wide column">
        <div class="ui relaxed divided list m-t-40">
          @include('agencypages.clients.includes.clientbox')
          @include('agencypages.clients.includes.clientbox')
        </div>
      </div>

      <div class="six wide column" style="display:none">
        <a class="ui right floated primary button" href="{{ route('agency.clients.invite') }}">
          <i class="plus icon"></i>
          Invite a client
        </a>
        
        <h4>Invited</h4>

        @if (session('success'))
          <div class="ui success message">
            <div class="header">Success</div>
            <p>{{ session('success') }}</p>
          </div>
        @endif

        @if ($invites->count() > 0)
          <div class="ui relaxed divided list m-t-40">
            @foreach ($invites->reverse() as $invite)
              @include('agencypages.clients.includes.invitebox', $invite)
            @endforeach
          </div>
        @endif
      </div>
    </div>

  </div>
@endsection


@section('script-footer')
  <script>
    $('.dropdown')
      .dropdown({
      })
    ;
  </script>
@endsection