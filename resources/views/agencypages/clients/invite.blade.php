@extends('layouts.agency-dashboard')


@section('title')
Invite a Client - {{ config('app.name') }}
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <a class="section" href="{{ route('agency.clients') }}">Client List</a>
      <span class="divider">/</span>
      <div class="active section">Invite a Cient</div>
    </div>

    <div class="ui container segment basic">
      @include('agencypages.clients.includes.inviteform')
    </div>
  </div>
@endsection


@section('script-footer')
  <script>
    $(document).ready(function () {
      $('#form_loader').submit(function(e){
        $(this).addClass('loading');
      });
    });
  </script>
@endsection