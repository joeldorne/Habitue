<div class="item">
  <div class="right floated content icon">
    <div class="ui text menu">
      <div class="ui left pointing right dropdown item">
        <i class="small ellipsis horizontal icon"></i>
        <div class="menu">
          <div class="item"><i class="building icon"></i>Profile</div>
          <div class="item"><i class="edit icon"></i>Edit</div>
          <div class="item"><i class="red times icon"></i>Remove</div>
        </div>
      </div>
    </div>
  </div>
  <i class="building icon"></i>
  <div class="content">
    <p class="header">{{ $invite->institution_name }}</p>
    <p class="header">{{ $invite->contact_name }}</p>
    <div class="description">{{ $invite->email }}</div>
  </div>
</div>