@extends('layouts.auth_all')


@section('title')
Group Profile
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main container segment basic">
        <p>Group Name: {{ Auth::user()->myGroup()->name }}</p>
        <p>Website: {{ Auth::user()->myGroup()->website_url }}</p>
        <p>Country: {{ Auth::user()->myGroup()->country }}</p>
        <p>Phone: {{ Auth::user()->myGroup()->phone }}</p>
        <p>Subdomain: {{ Auth::user()->myGroup()->subdomain }}</p>

        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <p><a href="{{ route('agency.profile.edit') }}">Update Profile</a></p>
    </div>
@endsection


@section('script-footer')
@endsection