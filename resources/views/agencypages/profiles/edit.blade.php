@extends('layouts.auth_all')


@section('title')
Group Profile Change
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main container segment basic">
        <div class="ui small form {{ ($errors->any()) ? 'error' : '' }} {{ (session('success')) ? 'success' : '' }}">

            <form method="post" action="{{ route('agency.profile.update') }}">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $group->id }}">


                <div class="three fields">
                    <div class="field"></div>
                    <div class="required field">
                        <label>Group name</label>
                        <input type="text" name="name" placeholder="the name of your group" value="{{ (old('name'))?old('name'):$group->name }}" required>
                    </div>
                    <div class="field"></div>
                </div>

                <div class="three fields">
                    <div class="field"></div>
                    <div class="field">
                        <label>Phone</label>
                        <input type="text" name="phone" placeholder="phone" value="{{ (old('phone'))?old('phone'):$group->phone }}">
                    </div>
                    <div class="field"></div>
                </div>

                <div class="three fields">
                    <div class="field"></div>
                    <div class="field">
                        <label>Website</label>
                        <div class="ui labeled input">
                            <div class="ui label">
                                http://
                            </div>
                            <input type="text" name="website" placeholder="website" value="{{ (old('website'))?old('website'):$group->website_url }}">
                        </div>
                    </div>
                    <div class="field"></div>
                </div>

                <div class="three fields">
                    <div class="field"></div>
                    <div class="field">
                        <label>Country</label>
                        @include('components.country-select-plain', ['selected'=>$group->country])
                    </div>
                    <div class="field"></div>
                </div>

                <div class="three fields">
                    <div class="field"></div>
                    <div class="field">
                        <label>Subdomain</label>
                        <div class="ui labeled input">
                            <input type="text" name="subdomain" placeholder="subdomain" value="{{ (old('subdomain'))?old('subdomain'):$group->subdomain }}" maxlength="15" required>
                            <div class="ui label">
                                .habitue.com
                            </div>
                        </div>
                    </div>
                    <div class="field"></div>
                </div>

                <div class="three fields">
                    <div class="field"></div>
                    <div class="field">
                        <input type="submit" id="btn_submit" class="ui submit orange huge button" value="Change">
                    </div>
                    <div class="field"></div>
                </div>

            </form>

            @if($errors->any() || true)
                <div class="ui error message">
                    <div class="header">There were some errors with your update.</div>
                    <ul class="list">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif

        </div>
    </div>
@endsection


@section('script-footer')
@endsection