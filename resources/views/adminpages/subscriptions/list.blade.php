@extends('layouts.auth_all')


@section('title')
Subscriptions
@endsection


@section('script-header')
@endsection


@section('content')
    @include('adminpages.subscriptions.partials.breadcrumbs')

    <div class="ui main fluid container segment basic">
    
        @include('adminpages.subscriptions.partials.list-tablelist', $subscriptions)

        <!-- @foreach ($subscriptions as $subscription)
            <p>{{ $subscription->name }} - {{ $subscription->image }} - {{ $subscription->disabled }} - {{ $subscription->description }}</p>
        @endforeach -->

    </div>
@endsection


@section('script-footer')
@endsection