@extends('layouts.auth_all')


@section('title')
Migration
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main container segment basic">

        <h3>Stripe Migration</h3>

        <a href="{{ route('admin.settings.stripe.migration.productService') }}" class="ui button primary">Migrate Product-Subscriptions</a>


    </div>
@endsection


@section('script-footer')
@endsection