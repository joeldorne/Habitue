@extends('layouts.auth_all')


@section('title')
Product
@endsection


@section('script-header')
@endsection


@section('content')
    @include('adminpages.products.partials.breadcrumbs')

    <div class="ui main fluidx container segment basic">


        <product-crud-container :receivingproduct="{{$product}}" :setmode="'view'"></product-crud-container>


    </div>
@endsection


@section('script-footer')
@endsection