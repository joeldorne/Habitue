@extends('layouts.auth_all')


@section('title')
Edit Product
@endsection


@section('script-header')
    <!-- <link href="{{URL::asset('vendor/cropper/cropper.min.css')}}" rel="stylesheet" type="text/css" /> -->
@endsection


@section('content')
    @include('adminpages.products.partials.breadcrumbs')

    <div class="ui main fluidx container segment basic">


        <product-crud-container :receivingproduct="{{$product}}" :setmode="'edit'"></product-crud-container>


    </div>
@endsection


@section('script-footer')
    <!-- <script src="{{URL::asset('vendor/cropper/cropper.min.js')}}"></script> -->
@endsection