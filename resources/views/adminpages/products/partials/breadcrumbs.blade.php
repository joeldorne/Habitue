@if (\Request::route()->getName() == 'admin.products')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        Products 
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'admin.product.create')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        <a class="section" href="{{ route('admin.products') }}">Products</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        Create New Product
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'admin.product.edit')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        <a class="section" href="{{ route('admin.products') }}">Products</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        Edit Product
      </div>
    </div>
  </div>

@elseif(\Request::route()->getName() == 'admin.product.view')

  <div class="ui main fluid segment basic">
    <div class="ui breadcrumb">
      <a class="section" href="{{ route('dashboard') }}">Dashboard</a>
      <span class="divider">/</span>
      <div class="active section">
        <i class="tv icon"></i> 
        <a class="section" href="{{ route('admin.products') }}">Products</a>
      </div>
      <span class="divider">/</span>
      <div class="active section">
        Edit Product
      </div>
    </div>
  </div>

@endif