@if ($role == 'agency')

  <h1 class="ui fluid teal header">
    <div class="content">
      Signup to {{ config('app.name') }} as {{ ucwords($role) }}
    </div>
  </h1>

  <h5 class="ui fluid header">
    <div class="sub header">or</div>
  </h5>

  <h3 class="ui fluid blue header">
    <div class="content">
      <a href="/register?role=client" class="ui blue tag label">Signup as Client</a>
    </div>
  </h3>

@else

  <h1 class="ui fluid blue header">
    <div class="content">
      Signup to {{ config('app.name') }} as {{ ucwords($role) }}
    </div>
  </h1>

  <h5 class="ui fluid header">
    <div class="sub header">or</div>
  </h5>

  <h3 class="ui fluid teal header">
    <div class="content">
      <a href="/register?role=agency" class="ui teal tag label">Signup as Agency</a>
    </div>
  </h3>

@endif