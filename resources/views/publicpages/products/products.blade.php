@extends('layouts.public')


@section('title')
Products
@endsection


@section('script-header')
@endsection


@section('content')
  <div class="ui main fluidx container segment basic">

    <h3>{{ $agency->name }} Products ({{ $agency->public_products()->count() }})</h3>



    <div class="ui link cards">

      @foreach($agency->public_products as $product)
        
        @include('publicpages.products.includes.product-box', $product)
      
      @endforeach
    
    </div>
    
  </div>
@endsection


@section('script-footer')
@endsection