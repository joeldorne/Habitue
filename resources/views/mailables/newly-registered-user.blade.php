@component('mail::message')
# Introduction

Welcome {{$user->fullname}},.

@component('mail::button', ['url' => route('dashboard')])
Visit Dashboard
@endcomponent

@component('mail::panel')
This is the panel content.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
