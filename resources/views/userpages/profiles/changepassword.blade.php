@extends('layouts.auth_all')


@section('title')
Change Password
@endsection


@section('script-header')
@endsection


@section('content')
    <div class="ui main container segment basic">
        <div class="ui small form {{ ($errors->any()) ? 'error' : '' }} {{ (session('success')) ? 'success' : '' }}">
        
            <form method="post" action="{{ route('user.profile.password.update') }}">
                {{ csrf_field() }}
                
                <div class="ui segment basic">
                    
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i> 
                            <input type="password" name="currentpassword" placeholder="CurrentPassword" required="required">
                        </div>
                    </div> 
                    
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i> 
                            <input type="password" name="password" placeholder="NewPassword" required="required">
                        </div>
                    </div> 
                    
                    <div class="field">
                        <div class="ui left icon input">
                            <i class="lock icon"></i> 
                            <input type="password" name="password_confirmation" placeholder="Repeat New Password" required="required">
                        </div>
                    </div> 
                    
                    <button type="submit" id="btn_submit" class="ui fluid large teal submit button">Change</button>
                </div>
            </form>

            @if($errors->any() || true)
                <div class="ui error message">
                    <div class="header">Error</div>
                    <ul class="list">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif

        </div>
    </div>
@endsection


@section('script-footer')
@endsection